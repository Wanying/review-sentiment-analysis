/**
 * 
 */
package entity;

/**
 * @author Wanying
 * @email wd78@drexel.edu
 *
 */
public class ItemScore implements Comparable {

	/**
	 * @param args
	 */
	
	private int id;
	private double score;
	
	public ItemScore(int id, double score){
		this.id=id;
		this.score=score;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
	
	public int compareTo(Object obj){
		ItemScore item = (ItemScore)obj;
		if(item.score>this.score) return 1;
		else if(item.score==this.score) return 0;
		else return -1;
	}

}
