/**
 * 
 */
package entity;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 *
 *
 *This class is used to describe the word pair
 */
public class WordPair {

	/**
	 * @param args
	 */
	private int iid;
	private int jid;
	
	
	public WordPair(int iid, int jid){
		this.iid=iid;
		this.jid=jid;
	}
	
	public int getFirstWordID(){
		return iid;
	}
	
	public int getSecondWordID(){
		return jid;
	}
	
	
	public boolean equals(Object obj){
		WordPair wp = (WordPair)obj;
		if((wp.iid==this.iid)&&(wp.jid==this.jid)) return true;
		else if((wp.iid==this.jid)&&(wp.jid==this.iid)) return true;
		else return false;
	}
	
	public int hashCode(){
		int result=31;
		result+=this.iid*17;
		result+=this.jid*17;
		return result;
	}
	
	public String toString(){
		return iid+"-"+jid;
	}

}
