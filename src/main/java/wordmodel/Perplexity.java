/**
 * 
 */
package wordmodel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 *
 */
public class Perplexity {

	/**
	 * This class is used to calculate the perplexity of this model
	 * p(w)=p(z|d)*p(s|z)*p(w|s,z);
	 * 
	 * perplexity(test set w)=exp{-p(w)/count-of-tokens}
	 * 
	 * the lower perplexity is ,the better result is 
	 */
	
	private GibbsSampling gs;
	
	public Perplexity(GibbsSampling gs){
		this.gs=gs;
		
		
	}
	
	public double getPerplexity(String confFile) throws IOException{
		double perplexity =-1;
		
		double loglik=0;
		double zeta=0.01;
		List<TopicStatus> topicList = gs. topicList ;
        
        /* p(w)=p(z|d)*p(s|z)*p(w|s,z);
        * perplexity(test set w)= exp{-p(w)/count-of-tokens}
        */
        int sumOfWords=0;
        for(int d=0;d<gs. numberOfDocument;d++){
              DocumentStatus ds = gs. documentList [d];
              sumOfWords+=ds.getDocumentLength();
              WordStatus[] wordList = ds. wordList;
              int [] dtd= new int[gs. numberOfTopic];
              for(int w=0;w<wordList. length;w++){
            	  WordStatus ws = wordList[w];
                  int topicAssignment = ws.topicAssignment ;
                   dtd[topicAssignment]++;
              }
              for(int w=0;w<wordList. length;w++){
                     //the likelihood of one word p(w)=p(w|s,z)*p(s|z)*p(z|d)
                  double simSum=0.0;
                  WordStatus ws = wordList[w];
                  int wid=ws.getWid();
                  for(int t=0;t<gs. numberOfTopic;t++){
                	  //p(z|d)
                	  double pzd=(dtd[t]+zeta)/(ds.getDocumentLength()+gs.numberOfTopic *zeta);
                      double deltaSum=gs.delta [0]+gs.delta [1]+gs.delta [2];
                      for (int s=0;s<gs. numberOfSentiment;s++){	
                    	  //p(s|z)
                    	  double psz=(topicList.get(t).sw [s]+gs.delta [s])/(topicList.get(t). numberOfWord+deltaSum);
                    	  //p(w|s,z)
                    	  double pwsz=(topicList.get(t).tsw [s][wid]+gs. epsilon)/(topicList.get(t). sw[s]+gs. numberOfWord *gs.epsilon );
                    	  double sim=pzd*psz*pwsz;
                    	  simSum+=sim;
                      }
                  }
                  loglik+=Math. log(simSum);
              }
       }
       perplexity=Math. exp(-loglik/( double)sumOfWords);
       
       File file= new File(confFile);
       FileWriter fw = new FileWriter(file, true);
       BufferedWriter bw = new BufferedWriter(fw);
       bw.newLine();
       bw.write( "Perplexity: " +perplexity);
       bw.newLine();
       bw.flush();
       bw.close();
       
       return perplexity;
}

	public static void main(String[] args) {

	}

}
