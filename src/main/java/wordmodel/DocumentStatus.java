package wordmodel;

import java.util.ArrayList;
import java.util.List;

public class DocumentStatus {
	/*
	 * This class is used to record the document status
	 * */
	
	private int did;
	public WordStatus[] wordList;
	private int documentLength;
	public int numberOfTable;
	public List<TableStatus> tableList;
	
	public DocumentStatus(int id, int documentLength){
		this.did=id;
		this.documentLength=documentLength;
		
		this.numberOfTable=0;
		this.wordList = new WordStatus[documentLength];
		
		this.tableList= new ArrayList<TableStatus>();
	}
	
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	public int getDocumentLength() {
		return documentLength;
	}
	public void setDocumentLength(int documentLength) {
		this.documentLength = documentLength;
	}
	
	public String toString(){
		String str = did+","+documentLength+",";
		for(int w=0;w<documentLength;w++){
			str+=wordList[w].toString()+" ";
		}
		return str.trim();
	}
	
	public void addTable(TableStatus ts){
		this.tableList.add(ts);
	}
	public void removeTable(TableStatus ts){
		int tid = ts.getTid();
		this.tableList.remove(ts);
		for(int t=0;t<tableList.size();t++){
			int id = tableList.get(t).getTid();
			if(id>tid){
				this.tableList.get(t).setTid(id-1);
			}
		}
		for(int w=0;w<this.wordList.length;w++){
			if(this.wordList[w].tableAssignment>tid){
				this.wordList[w].tableAssignment--;
			}
		}
		this.numberOfTable--;
	}
}
