/**
 * 
 */
package wordmodel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import entity.ItemScore;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 *
 */
public class ModelSave {

	/**
	 * This method is used to store the generated data
	 */
	
	private GibbsSampling gs;
	public HashMap<String,Integer> W2ID;
	public HashMap<Integer,String> ID2W;
	public double zeta=0.01;
	
	public ModelSave(GibbsSampling gs){
		this.gs=gs;
		this.W2ID=gs.W2ID;
		this.ID2W=gs.ID2W;
	}
	
	public ModelSave(GibbsSampling gs, String confFile,String topicListFile,String topicDir,String topicFile, String documentFile) throws IOException{
		this.gs=gs;
		this.W2ID=gs.W2ID;
		this.ID2W=gs.ID2W;
		saveConfiguration(confFile);
		saveTopicList(topicListFile);
		saveTopicFiles(topicDir,topicFile);
		saveDocumentFiles(documentFile);	
	}
	
	public void saveConfiguration(String configurationFile) throws IOException{
		BufferedWriter bw =modelWriter(configurationFile);
		JSONObject obj = new JSONObject();
		obj.put("numberOfTopic", gs.numberOfTopic);
		obj.put("numberOfUpperLimitTopic", gs.numberOfUpperLimitTopic);
		obj.put("numberOfSentiment", gs.numberOfSentiment);
		obj.put("alpha", gs.alpha);
		obj.put("beta", gs.beta);
		obj.put("gamma", gs.gamma);
		obj.put("epsilon", gs.epsilon);
		obj.put("iteration", gs.iter);
		bw.write(obj.toString());
		bw.flush();
		shutdownWriter(bw);
	}
	
	public void saveTopicList(String topicListFile) throws IOException{
		
		BufferedWriter bw = modelWriter(topicListFile);
		
		List<TopicStatus> topicList = gs.topicList;
		for(int t=0;t<topicList.size();t++){
			TopicStatus ts = topicList.get(t);
			JSONObject topicJSON = new JSONObject();
			topicJSON.put("topicID",ts.tid);
			topicJSON.put("wordList", Arrays.toString(ts.wordList));
			
			for(int s=0;s<gs.numberOfSentiment;s++){
				int[] sw = ts.tsw[s];
				topicJSON.put("sentiment_"+s+"_WordArray", Arrays.toString(sw));
				int sNum=ts.sw[s];
				topicJSON.put("sentiment_"+s+"_Sum", sNum);
			}
			bw.write(topicJSON.toString());
			bw.newLine();
			bw.flush();
		}
		
		shutdownWriter(bw);
	}
	public void saveTopicFiles(String topicDir, String topicFile) throws IOException{
		/*
		 * This method is used to save the word list for each topic
		 * words are ordered by their frequency
		 * */
		File fileDir = new File(topicDir);
		if(!(fileDir.exists())) fileDir.mkdir();
		int sumOfWords=0;
		for(int t=0;t<gs.topicList.size();t++){
			sumOfWords+=gs.topicList.get(t).numberOfWord;
		}
		BufferedWriter tbw = modelWriter(topicFile);
		
		ItemScore topicList[]= new ItemScore[gs.numberOfTopic];
		
		for(int t=0;t<gs.topicList.size();t++){
			TopicStatus ts = gs.topicList.get(t);
			int id =ts.tid;
			String fileName=topicDir+"/"+id+".txt";
			BufferedWriter bw = modelWriter(fileName);
			double topicProp = (double)ts.numberOfWord/(double)sumOfWords;
			ItemScore topicIS = new ItemScore(t,topicProp);
			topicList[t]=topicIS;
			bw.write("Topic Proportion of Topic "+t+" is: "+topicProp+"\n");
			//save representative topic words
			bw.write("Top 60 Representative Words in Topic "+t+":\n");
			ItemScore wordScoreList[] = new ItemScore[gs.numberOfWord];
			for(int w=0;w<ts.wordList.length;w++){
				double score = (ts.wordList[w]+gs.beta)/(ts.numberOfWord+gs.numberOfWord*gs.beta);
				ItemScore is = new ItemScore(w,score);
				wordScoreList[w]=is;
			}
			Arrays.sort(wordScoreList);
			
			for(int w=0;w<60;w++){
				ItemScore is = wordScoreList[w];
				int wid = is.getId();
				double score=is.getScore();
				String word = ID2W.get(wid);
				bw.write(word+":"+score);
				bw.newLine();
				bw.flush();
			}
			bw.newLine();
			bw.flush();
			//save sentiment words
			for(int s=0;s<gs.numberOfSentiment;s++){
				
				double senProp = ((double)ts.sw[s]/(double)ts.numberOfWord);
				bw.write("Sentiment "+s+" Proportion: "+senProp+"\n");
				
				ItemScore[] itemScoreList = new ItemScore[gs.numberOfWord];
				for(int w=0;w<gs.numberOfWord;w++){
					double score = (ts.tsw[s][w]+gs.epsilon)/(ts.sw[s]+gs.numberOfWord*gs.epsilon);
					ItemScore is = new ItemScore(w,score);
					itemScoreList[w]=is;
				}
				Arrays.sort(itemScoreList);
				bw.write("Most Representative 20 Words in Sentiment "+s+":\n");
				for(int w=0;w<20;w++){
					ItemScore is = itemScoreList[w];
					int wid = is.getId();
					String word = ID2W.get(wid);
					double score = is.getScore();
					bw.write(word+":"+score);
					bw.newLine();
					bw.flush();
				}
				bw.newLine();
				bw.flush();
			}
			shutdownWriter(bw);
		}
		Arrays.sort(topicList);
		for(int t=0;t<topicList.length;t++){
			tbw.write("Topic "+topicList[t].getId()+" : "+topicList[t].getScore());
			tbw.newLine();
			tbw.flush();
		}
		shutdownWriter(tbw);
	}
	
	public void saveDocumentFiles(String documentFile) throws IOException{
		/*
		 * Each Documents Distribution over Topics and Sentiment
		 * */
		BufferedWriter bw = modelWriter(documentFile);
		for(int d=0;d<gs.documentList.length;d++){
			String doc="";
			DocumentStatus ds = gs.documentList[d];
			WordStatus[] wordList=ds.wordList;
			int topic[] = new int[gs.numberOfTopic];
			int sentiment[][] = new int[gs.numberOfTopic][gs.numberOfSentiment];
			for(int w=0;w<wordList.length;w++){
				int topicAssignment = wordList[w].topicAssignment;
				int sentimentAssignment = wordList[w].sentimentAssignment;
				topic[topicAssignment]++;
				sentiment[topicAssignment][sentimentAssignment]++;
			}
			
			ItemScore[] topicList = new ItemScore[gs.numberOfTopic];
			ItemScore[][] sentimentList = new ItemScore[gs.numberOfTopic][gs.numberOfSentiment];
			
			for(int t=0;t<gs.numberOfTopic;t++){
				double topicScore =(topic[t]+zeta)/(ds.getDocumentLength()+gs.numberOfTopic*zeta);
				ItemScore tis = new ItemScore(t,topicScore);
				topicList[t]=tis;
				
				int tsw[] = sentiment[t];
				double deltaSum=gs.delta[0]+gs.delta[1]+gs.delta[2];
				for(int s=0;s<gs.numberOfSentiment;s++){
					double sentimentScore = (tsw[s]+gs.delta[s])/(topic[t]+gs.numberOfSentiment*deltaSum);
					ItemScore sis = new ItemScore(s,sentimentScore);
					sentimentList[t][s]=sis;
				}
				Arrays.sort(sentimentList[t]);
			}
			Arrays.sort(topicList);
			
			for(int t=0;t<gs.numberOfTopic;t++){
				doc+="Topic "+topicList[t].getId()+":"+topicList[t].getScore()+";";
				for(int s=0;s<gs.numberOfSentiment;s++){
					doc+="Sentiment "+sentimentList[t][s].getId()+":"+sentimentList[t][s].getScore()+";";
				}
			}
			bw.write(doc+"\n");
			bw.flush();
		}
		bw.flush();
		bw.close();
		
	}
	
	private BufferedWriter modelWriter(String fileAddr) throws IOException{
		File file = new File(fileAddr);
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		return bw;
	}
	
	private void shutdownWriter(BufferedWriter bw) throws IOException{
		bw.flush();
		bw.close();
	}
	public static void main(String[] args) throws IOException {
		String dir ="D:/JavaProject/Sentiment Analysis/dataset/data/";
		String wordFile=dir+"test.txt";
		String wordMap=dir+"wordmap.txt";
		String simMap = dir+"simmap.txt";
		String senMap = dir+"sentiment.txt";
		Documents documents = new Documents(wordFile,wordMap,simMap,senMap);
		
		DocumentStatus[] documentList = new DocumentStatus[documents.numberOfDocuments/2];
		for(int d=0;d<documents.numberOfDocuments/2;d++){
			documentList[d]=documents.documentList[d];
		}
		
		
		
		int K=10;
		int UK =100;
		int S=3;
		
		Model model = new Model(documents,K,UK,S);
		GibbsSampling gs = new GibbsSampling();
		gs.loadModel(model);
		gs.run();
		
		ModelSave ms = new ModelSave(gs);
		
		String configurationFile = dir+"conf.txt";
		String topicList = dir+"topicList.txt";
		String topicDir = dir+"topicDir";
		String documentFile=dir+"documentFile.txt";
		String topicFile=dir+"topicFile.txt";
		
		ms.saveConfiguration(configurationFile);
		ms.saveTopicFiles(topicDir,topicFile);
		ms.saveTopicList(topicList);
		ms.saveDocumentFiles(documentFile);
		
		
		

	}

}
