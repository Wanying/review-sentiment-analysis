/**
 * 
 */
package wordmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.json.JSONObject;

import entity.WordPair;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 *
 */
public class Model {

	/**
	 * @param args
	 */
	public int numberOfDocument;
	public int numberOfWord;
	public int numberOfTopic;
	public int numberOfLimitTopic;
	public int numberOfSentiment;
	
	public double alpha;
	public double beta;
	public double gamma;
	public double delta[];
	public double epsilon;
	
	public HashMap<Integer,String> ID2W;
	public HashMap<String,Integer> W2ID;
	public HashMap<WordPair,Double> SIMMAP;
	public HashMap<Integer,double[]> SENMAP;
	
	public DocumentStatus[] documentList;
	public List<TopicStatus> topicList;
	public int iter;
	
	private Random random = new Random();
	
	
	public Model(Documents documents, int initK,int upperK, int initS){
		readDocuments(documents);
		initModel(initK,upperK, initS);
	}
	private void readDocuments(Documents documents){
		this.numberOfDocument=documents.numberOfDocuments;
		this.numberOfWord=documents.numberOfWords;
		this.ID2W=documents.ID2W;
		this.W2ID=documents.W2ID;
		this.SIMMAP=documents.SIMMAP;
		this.SENMAP=documents.SENMAP;
		this.documentList=documents.documentList;
	}
	
	public void initModel(String configurationFile, String topicListFile) throws IOException{
		
		this.topicList= new ArrayList<TopicStatus>();
		
		File conFile = new File(configurationFile);
		FileReader confr = new FileReader(conFile);
		Scanner conscan = new Scanner(confr);
		
		while(conscan.hasNext()){
			JSONObject obj = new JSONObject(conscan.nextLine().trim());
			this.alpha=obj.getDouble("alpha");
			this.beta=obj.getDouble("beta");
			this.gamma=obj.getDouble("gamma");
			this.epsilon=obj.getDouble("epsilon");
			this.delta=new double[3];
			this.numberOfSentiment=obj.getInt("numberOfSentiment");
			this.numberOfTopic=obj.getInt("numberOfTopic");
			this.iter=obj.getInt("iteration");
		}
		
		File topicFile = new File(topicListFile);
		FileReader topicReader = new FileReader(topicFile);
		Scanner topicScanner = new Scanner(topicReader);
		
		while(topicScanner.hasNext()){
			String line = topicScanner.nextLine().trim();
			JSONObject obj = new JSONObject(line);
			int topicID = obj.getInt("topicID");
			TopicStatus ts = new TopicStatus(topicID,this.numberOfWord,this.numberOfSentiment);
			//set word set
			String wordSetStr = obj.getString("wordSet");
			wordSetStr=replaceSquareBracket(wordSetStr);
			//set word list
			String wordListStr = obj.getString("wordList");
			wordListStr = replaceSquareBracket(wordListStr);
			String listItems[]=wordListStr.split(",");
			for(int w=0;w<listItems.length;w++){
				String item = listItems[w].trim();
				ts.wordList[w]=Integer.parseInt(item);
			}
			
			//set sentiment list
			for(int s=0;s<numberOfSentiment;s++){
				String tswStr = obj.getString("sentiment_"+s+"_WordArray");
				tswStr=replaceSquareBracket(tswStr);
				String senItems[]=tswStr.split(",");
				for(int w=0;w<senItems.length;w++){
					String item = senItems[w].trim();
					ts.tsw[s][w]=Integer.parseInt(item);
				}
				int senNum = obj.getInt("sentiment_"+s+"_Sum");
				ts.sw[s]=senNum;
			}
			topicList.add(ts);
		}
		
		for(int d=0;d<documentList.length;d++){
			DocumentStatus ds = documentList[d];
			WordStatus[] wordList = ds.wordList;
			for(int w=0;w<wordList.length;w++){
				WordStatus ws = wordList[w];
				int wid=ws.getWid();
				
				//assign a table, topic and sentiment
				int tableAssignment = documentList[d].numberOfTable;
				int topicAssignment = random.nextInt(numberOfTopic);
				int sentimentAssignment = random.nextInt(numberOfSentiment);
				
				//word level
				documentList[d].wordList[w].tableAssignment=tableAssignment;
				documentList[d].wordList[w].topicAssignment=topicAssignment;
				documentList[d].wordList[w].sentimentAssignment=sentimentAssignment;
				//table level
				TableStatus ts = new TableStatus(tableAssignment,d,documentList[d].getDocumentLength());
				ts.numberOfWord++;
				ts.wordVector.setEntry(w, 1);
				ts.topicAssignment=topicAssignment;
				documentList[d].addTable(ts);
				documentList[d].numberOfTable++;
				//topic level
				topicList.get(topicAssignment).numberOfWord++;
				topicList.get(topicAssignment).wordList[wid]++;
				//sentiment level
				topicList.get(topicAssignment).tsw[sentimentAssignment][wid]++;
				topicList.get(topicAssignment).sw[sentimentAssignment]++;
			}
		}
	}
	
	private void initModel(int initK,int upperK,int initS){
		/*
		 * Randomly assign words to different topic and sentiment allocation
		 * Give an initK to numberOfTopic, and this value will change
		 * Give an initS(3) to numberOfSentiment, and this value will not change, 
		 * */
		this.numberOfTopic=initK;
		this.numberOfSentiment=initS;
		this.numberOfLimitTopic=upperK;
		this.topicList=new ArrayList<TopicStatus>();
		
		this.alpha=1E-8;
		this.beta=0.05;
		this.gamma=1E-8;
		this.delta=new double[numberOfSentiment];
		this.epsilon=0.001;
		this.iter=1000;
		
		/*
		 * Initiate topic assignment
		 * */
		for(int t=0;t<numberOfTopic;t++){
			TopicStatus ts = new TopicStatus(t,numberOfWord,numberOfSentiment);
			topicList.add(ts);
		}
		
		for(int d=0;d<documentList.length;d++){
			//get a document;
			int wordLength=documentList[d].getDocumentLength();
			for(int w=0;w<wordLength;w++){
				//get a word
				WordStatus ws = documentList[d].wordList[w];
				int wid=ws.getWid();
				//then I will assign this word to different topic and sentiment
				
				//assign a table, topic and sentiment
				int tableAssignment = documentList[d].numberOfTable;
				int topicAssignment = random.nextInt(numberOfTopic);
				int sentimentAssignment = random.nextInt(numberOfSentiment);
				
				//word level
				documentList[d].wordList[w].tableAssignment=tableAssignment;
				documentList[d].wordList[w].topicAssignment=topicAssignment;
				documentList[d].wordList[w].sentimentAssignment=sentimentAssignment;
				//table level
				TableStatus ts = new TableStatus(tableAssignment,d,documentList[d].getDocumentLength());
				ts.numberOfWord++;
				ts.wordVector.setEntry(w, 1);
				ts.topicAssignment=topicAssignment;
				documentList[d].addTable(ts);
				documentList[d].numberOfTable++;
				//topic level
				topicList.get(topicAssignment).numberOfWord++;
				topicList.get(topicAssignment).wordList[wid]++;
				//sentiment level
				topicList.get(topicAssignment).tsw[sentimentAssignment][wid]++;
				topicList.get(topicAssignment).sw[sentimentAssignment]++;
			}
		}
	}
	
	private String replaceSquareBracket(String str){
		str=str.replace("[", "");
		str=str.replace("]", "");
		str=str.trim();
		return str;
	}
	
	public static void main(String args[]) throws IOException{
		String dir ="D:/JavaProject/Sentiment Analysis/dataset/data/";
		String wordFile=dir+"word.txt";
		String wordMap=dir+"wordmap.txt";
		String simMap = dir+"simmap.txt";
		String senMap = dir+"sentiment.txt";
		Documents documents = new Documents(wordFile,wordMap,simMap,senMap);
		
		int UK=100;
		int K=10;
		int S=3;
		
		Model model = new Model(documents, K,UK,S);
		DocumentStatus documentList[]=  model.documentList;
		for(int d=0;d<documentList.length;d++){
			System.out.println(documentList[d].toString());
		}
	}

}
