/**
 * 
 */
package wordmodel;

import java.util.Vector;

import org.apache.commons.math3.linear.ArrayRealVector;

/**
 * @author Wanying
 * @email wd78@drexel.edu
 * 
 * This is class is used to define the status of words
 *
 */
public class WordStatus {

	/**
	 * @wid: word id
	 * @opn: whether this word in sentiment set, 1: in, 0: not in
	 * @topicAssignment: to which topic this word has been assigned to 
	 * @sentimentAssignment: to which sentiment this word has been assigned to
	 */
	private int wid;
	private double positive;
	private double negative;
	private double neutral;
	public int tableAssignment;
	public int topicAssignment;
	public int sentimentAssignment;
	
	private ArrayRealVector simVector;
	
	

	public WordStatus(int wid){
		this.wid=wid;
		this.positive=-1;
		this.negative=-1;
		this.neutral=-1;
		this.tableAssignment=-1;
		this.topicAssignment=-1;
		this.sentimentAssignment=-1;
	}
	
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	
	public double getPositive() {
		return positive;
	}

	public void setPositive(double positive) {
		this.positive = positive;
	}

	public double getNegative() {
		return negative;
	}

	public void setNegative(double negative) {
		this.negative = negative;
	}

	public double getNeutral() {
		return neutral;
	}

	public void setNeutral(double neutral) {
		this.neutral = neutral;
	}

	public ArrayRealVector getSimVector() {
		return simVector;
	}

	public void setSimVector(ArrayRealVector simVector) {
		this.simVector = simVector;
	}
	public String toString(){
		return wid+";"+tableAssignment+";"+topicAssignment+";"+sentimentAssignment;
	}
}
