/**
 * 
 */
package wordmodel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.omg.CORBA.DoubleSeqHelper;

import entity.WordPair;

/**
 * @author Wanying
 * @email:wd78@drexel.edu
 * 
 * This class is the main class for gibbs sampling
 *
 */

public class GibbsSampling {

	/**
	 * Because the parameters here will be called by other programs for a lot of times
	 * so, I will set them as public here
	 * 
	 * number of topic: not sure
	 * number of sentiment: 3
	 */
	
	public double alpha;// for table generation 
	public double gamma; // for topic generation
	public double beta;// for word-topic distribution
	public double delta[]; // for topic-sentiment-word distribution
	public double epsilon;
	
	public int numberOfDocument;
	public int numberOfWord;
	public int numberOfTopic;
	public int numberOfSentiment;
	public int numberOfUpperLimitTopic;
	
	public List<TopicStatus> topicList;
	public DocumentStatus[] documentList;
	
	public HashMap<String,Integer>W2ID;
	public HashMap<Integer,String>ID2W;
	public HashMap<WordPair,Double> SIMMAP;
	public HashMap<Integer,double[]>SENMAP;
	public List<ArrayRealVector> SIMMATRIX;
	public int iter;
	
	private Random random = new Random();
	
	
	public GibbsSampling(){
		
	}
	
	public GibbsSampling(Model model){
		loadModel(model);
		run();
	}
	
	public void loadModel(Model model){
		this.numberOfDocument=model.numberOfDocument;
		this.numberOfTopic=model.numberOfTopic;
		this.numberOfUpperLimitTopic=model.numberOfLimitTopic;
		this.numberOfWord=model.numberOfWord;
		this.numberOfSentiment=model.numberOfSentiment;
		
		this.documentList=model.documentList;
		this.topicList=model.topicList;
		
		this.alpha=model.alpha;
		this.beta=model.beta;
		this.gamma=model.gamma;
		this.delta=model.delta;
		this.epsilon=model.epsilon;
		this.iter=model.iter;
		
		this.W2ID=model.W2ID;
		this.ID2W=model.ID2W;
		this.SIMMAP=model.SIMMAP;
		this.SENMAP=model.SENMAP;
		
		this.SIMMATRIX= new ArrayList<ArrayRealVector>();
		for(int i=0;i<numberOfWord;i++){
			ArrayRealVector vector = new ArrayRealVector(numberOfWord);
			
			for(int j=0;j<numberOfWord;j++){
				WordPair wp = new WordPair(i,j);
				if(SIMMAP.containsKey(wp)){
					vector.setEntry(j, SIMMAP.get(wp));
				}else{
					vector.setEntry(j, 0.0);
				}
			}
			SIMMATRIX.add(vector);
		}
	}
	
	public void run(){
		for(int i=0;i<iter;i++){
			System.out.print("WORD MODEL SAMPLING "+i+"/"+iter+": \t");
			sampling();
		}
	}
	
	private void sampling(){
		for(int d=0;d<numberOfDocument;d++){
			//System.out.println("IS PROCESSING DOCUMENT "+d+"...");
			for(int w=0;w<documentList[d].wordList.length;w++){
				//long time1 = System.nanoTime();
				decrement(d,w);
				//long time2 = System.nanoTime();
				//System.out.println("DECREMENT TIME CONSUMING..."+(time2-time1));
				int tableAssignment = tableSampling(d,w);
				//long time3=System.nanoTime();
				//System.out.println("TABLE ASSIGNMENT TIME CONSUMING..."+(time3-time2));
				int topicAssignment;
				if(tableAssignment==documentList[d].numberOfTable){
					// this is a new table
					topicAssignment = topicSampling(d,w);
					if(topicAssignment==this.numberOfTopic){
						TopicStatus ts = new TopicStatus(this.numberOfTopic,numberOfWord,numberOfSentiment);
						this.topicList.add(ts);
						this.numberOfTopic++;
					}
				}else{
					//this is an old table
					topicAssignment=documentList[d].tableList.get(tableAssignment).topicAssignment;
				}
				//long time4=System.nanoTime();
				//System.out.println("TOPIC ASSIGNMENT TIME CONSUMING..."+(time4-time3));
				int sentimentAssignment=sentimentSampling(d,w,topicAssignment);
				//long time5=System.nanoTime();
				//System.out.println("SENITMENT ASSIGNMENT TIME CONSUMING..."+(time5-time4));
				allocate(d,w,tableAssignment,topicAssignment,sentimentAssignment);
				//long time6 = System.nanoTime();
				//System.out.println("ALLOCATE TIME CONSUMING..."+(time6-time5));
			}
		}
		System.out.println("NUMBER OF TOPIC: "+numberOfTopic);
	}
	
	private void decrement(int did, int w){
		DocumentStatus ds = documentList[did];
		WordStatus ws = ds.wordList[w];
		int wid=ws.getWid();
		
		int tableAssignment = ws.tableAssignment;
		int topicAssignment = ws.topicAssignment;
		int sentimentAssignment = ws.sentimentAssignment;
		
		//table level
		documentList[did].tableList.get(tableAssignment).numberOfWord--;
		documentList[did].tableList.get(tableAssignment).wordVector.setEntry(w, 0);
		//topic level
		topicList.get(topicAssignment).numberOfWord--;
		topicList.get(topicAssignment).wordList[wid]--;
		//sentiment level
		topicList.get(topicAssignment).sw[sentimentAssignment]--;
		topicList.get(topicAssignment).tsw[sentimentAssignment][wid]--;
		
		//if the table is empty, remove this table
		if(documentList[did].tableList.get(tableAssignment).numberOfWord==0){
			documentList[did].removeTable(new TableStatus(tableAssignment,did));
		}
		//if the topic is empty, remove this topic
		if(topicList.get(topicAssignment).numberOfWord==0){
			//change topic id
			//System.out.println("REMOVING S TOPIC...");
			for(int d=0;d<numberOfDocument;d++){
				DocumentStatus doc = documentList[d];
				List<TableStatus> tableList = doc.tableList;
				for(int t=0;t<tableList.size();t++){
					if(tableList.get(t).topicAssignment>topicAssignment){
						documentList[d].tableList.get(t).topicAssignment--;
					}
				}
				WordStatus[] wordList = doc.wordList;
				for(int i=0;i<wordList.length;i++){
					if(wordList[i].topicAssignment>topicAssignment){
						documentList[d].wordList[i].topicAssignment--;
					}
				}
			}
			
			for(int t=topicAssignment+1;t<topicList.size();t++){
				topicList.get(t).tid--;
			}
			topicList.remove(topicAssignment);
			this.numberOfTopic--;
		}
	}
	
	private void allocate(int did, int w, int tableAssignment, int topicAssignment, int sentimentAssignment){
		DocumentStatus ds = documentList[did];
		WordStatus ws = ds.wordList[w];
		int wid=ws.getWid();
		
		//table level
		documentList[did].wordList[w].tableAssignment=tableAssignment;
		documentList[did].wordList[w].topicAssignment=topicAssignment;
		documentList[did].wordList[w].sentimentAssignment=sentimentAssignment;
		//First judge whether this table a new table or an old table
		if(tableAssignment==ds.numberOfTable){
			// this is a new table
			TableStatus ts = new TableStatus(tableAssignment,did,ds.getDocumentLength());
			ts.numberOfWord++;
			ts.wordVector.setEntry(w,1);
			ts.topicAssignment=topicAssignment;
			documentList[did].addTable(ts);
			documentList[did].numberOfTable++;
		}else{
			//this is an old table
			documentList[did].tableList.get(tableAssignment).topicAssignment=topicAssignment;
			documentList[did].tableList.get(tableAssignment).wordVector.setEntry(w, 1);
			documentList[did].tableList.get(tableAssignment).numberOfWord++;
		}
			
		//topic level & sentiment level
		//similarly,judge whether this topic is a new topic or an old one
		
		topicList.get(topicAssignment).numberOfWord++;
		topicList.get(topicAssignment).wordList[wid]++;
	
		topicList.get(topicAssignment).tsw[sentimentAssignment][wid]++;
		topicList.get(topicAssignment).sw[sentimentAssignment]++;
		
	}
	
	
	private int tableSampling(int did, int w){
		/*
		 * Each word will choose a table to sit
		 * Each word will scan over all the tables
		 * 	 and choose one it is most familiar with to sit
		 * */
		DocumentStatus ds = documentList[did];
		WordStatus ws = ds.wordList[w];
		int wid = ws.getWid();
		
		int table=-1;
		int numberOfTable =ds.numberOfTable;
		double[]tsim = new double[numberOfTable];
		double[]tdist = new double[numberOfTable];
		double[]tt = new double[numberOfTable+1];
		double pSum=0.0;
		double simSum=0;
		double u=0.0;
	
		ArrayRealVector simVector = ws.getSimVector();
		List<TableStatus> tableList = ds.tableList;
		for(int t=0;t<tableList.size();t++){
			TableStatus ts = tableList.get(t);
			int topicAssignment =ts.topicAssignment;
			TopicStatus  topic = topicList.get(topicAssignment);
			ArrayRealVector wordVector = ts.wordVector;
			double sim= simVector.dotProduct(wordVector)/((double)ts.numberOfWord);
			tsim[t]=sim;
			simSum+=sim;
			//for one topic, this sum of dist value should be 1
			tdist[t]=(topic.wordList[wid]+beta)/(topic.numberOfWord+numberOfWord*beta);
		}
		
		for(int t=0;t<tableList.size();t++){
			pSum+=(tsim[t]/(simSum+alpha))*tdist[t];
			tt[t]=pSum;
		}
		//G0: uniform distribution
		pSum+=(alpha/(simSum+alpha))*random.nextDouble();
		tt[numberOfTable]=pSum;
		u=random.nextDouble()*pSum;
		
		for(table=0;table<=numberOfTable;table++){
			if(tt[table]>=u) break;
		}
		return table;
	}
	
	private int topicSampling(int did, int w){
		/*
		 * This method is used to sample topic 
		 * Each table will choose a dish to eat
		 * Each table will each the dish which is shared by the most similar tables
		 * */
		int topic=-1;
		double simSum=0;
		double u=0.0;
		double tsim[]= new double[numberOfTopic];
		double tdis[]= new double[numberOfTopic];
		double tt[] = new double[numberOfTopic+1];
		double pSum=0.0;
		
		DocumentStatus ds = documentList[did];
		WordStatus ws = ds.wordList[w];
		int wid = ws.getWid();
		ArrayRealVector simVector = SIMMATRIX.get(wid);
		ArrayRealVector topicVector = new ArrayRealVector(numberOfWord);
		//iterate all the topic
		for(int t=0;t<topicList.size();t++){
			//long time1 = System.nanoTime();
			TopicStatus tos = topicList.get(t);
			double sim=0.0;
			double[] twordList = tos.wordList;
			topicVector = new ArrayRealVector(twordList);
			sim = simVector.dotProduct(topicVector)/((double)tos.numberOfWord);
			//sim=0.5;
			simSum+=sim;
			tsim[t]=sim;
			tdis[t]=(twordList[wid]+beta)/(tos.numberOfWord+numberOfWord*beta);
			//long time2=System.nanoTime();
			//System.out.println(time2-time1);
		}
	
		for(int t=0;t<topicList.size();t++){
			pSum+=(tsim[t]/(simSum+gamma))*tdis[t];
			tt[t]=pSum;
		}
		//if topic number is less than the limit, it can create topic
		//System.out.println("TOPIC NUMBER: "+this.numberOfTopic+" , "+this.numberOfUpperLimitTopic);
		if(numberOfTopic<this.numberOfUpperLimitTopic){
			pSum+=(gamma/(simSum+gamma))*random.nextDouble();
			tt[numberOfTopic]=pSum;
			u=random.nextDouble()*pSum;
			for(topic=0;topic<=topicList.size();topic++){
				if(tt[topic]>=u) break;
			}	
		}else{
			//if the topic number is larger than the limit, not allow to create topic any more
			u=random.nextDouble()*pSum;
			for(topic=0;topic<topicList.size();topic++){
				if(tt[topic]>=u) break;
			}
		}
	
		//System.out.println("Topic "+topic+", NOT: "+numberOfTopic+";"+topicList.size());
		return topic;
	}
	
	private int sentimentSampling(int did, int w,int topicAssignment){
		int sentiment=-1;
		double pSum=0;
		double ss[]=new double[numberOfSentiment];
		double u;
		
		DocumentStatus ds = documentList[did];
		WordStatus ws = ds.wordList[w];
		TopicStatus ts = topicList.get(topicAssignment);
		int wid = ws.getWid();
		delta[0]=ws.getPositive();
		delta[1]=ws.getNegative();
		delta[2]=ws.getNeutral();
		double deltaSum=delta[0]+delta[1]+delta[2];
		int[][]tsw = ts.tsw;
		int[] sw =ts.sw;
		for(int s=0;s<numberOfSentiment;s++){
			pSum+=((tsw[s][wid]+epsilon)/(sw[s]+numberOfWord*epsilon))*((sw[s]+delta[s])/(ts.numberOfWord+deltaSum));
			ss[s]=pSum;
		}
		u=random.nextDouble()*pSum;
		for(sentiment=0;sentiment<numberOfSentiment;sentiment++){
			if(ss[sentiment]>=u) break;
		}
		return sentiment;
	}
	
	public static void main(String[] args) throws IOException {
		String dir ="D:/JavaProject/Sentiment Analysis/dataset/data/wordmodel/data/";
		String wordFile=dir+"test.txt";
		String wordMap=dir+"wordmap.txt";
		String simMap = dir+"simmap.txt";
		String senMap = dir+"sentiment.txt";
		Documents documents = new Documents(wordFile,wordMap,simMap,senMap);

		int initK=10;
		int upperK=100;
		int S=3;
		
		Model model = new Model(documents,initK,upperK,S);
		GibbsSampling gs = new GibbsSampling();
		gs.loadModel(model);
		gs.run();

	}

}
