/**
 * 
 */
package wordmodel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 *
 */
public class TopicStatus {

	/**
	 * This class is used to record the topic status
	 */
	public int tid;
	public int numberOfWord;
	public double[] wordList;
	public int[][] tsw; // sentiment distribution 
	public int[] sw;	
	public TopicStatus(int tid,int numberOfWord, int numberOfSentiment){
		this.tid=tid;
		this.numberOfWord=0;
		this.wordList=new double[numberOfWord];
		this.tsw=new int[numberOfSentiment][numberOfWord];
		this.sw= new int[numberOfSentiment];
	}
	
}
