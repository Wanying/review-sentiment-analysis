/**
 * 
 */
package wordmodel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 * 
 * Because each document contains a number of tables
 * so we need to define the table status
 * 
 * A table is a local topic 
 *
 */
public class TableStatus {

	/**
	 * @tid: table id
	 * @did: to which document, this table belongs to
	 * @wordList: the words in this table, the Integer here, presents the position of this word
	 * @topicAssighment: connect to the global topic
	 */
	
	private int tid;
	private int did;
	public ArrayRealVector wordVector;
	public int topicAssignment;
	public int numberOfWord;
	
	public TableStatus(int tid, int did,int documentLength){
		this.tid=tid;
		this.did=did;
		this.wordVector= new ArrayRealVector(documentLength);//only 0 or 1
		for(int w=0;w<documentLength;w++){
			this.wordVector.setEntry(w, 0);
		}
		this.topicAssignment=-1;
		this.numberOfWord=0;
	}
	/*
	 *A short version of table status 
	 */
	public TableStatus(int tid, int did){
		this.tid=tid;
		this.did=did;
		this.wordVector=null;
		this.topicAssignment=0;
		this.numberOfWord=0;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	
	public int getDid() {
		return did;
	}
	public void setDid(int did) {
		this.did = did;
	}
	
	public boolean equals(Object obj){
		TableStatus ts = (TableStatus)obj;
		if((ts.did==this.did)&&(ts.tid==this.tid)) return true;
		else return false;
	}
	
	public int hashCode(){
		int result =31;
		result+=this.tid*17;
		result+=this.did*17;
		
		return result;
	}
	
	
}
