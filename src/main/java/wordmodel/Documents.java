/**
 * 
 */
package wordmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.json.JSONObject;

import entity.WordPair;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 * 
 * This class is used store any documents that might be used later into memory
 *
 */
public class Documents {

	/**
	 * @param args
	 */
	public List<String> docList = new ArrayList<String>();
	public HashMap<Integer,String> ID2W = new HashMap<Integer,String>();
	public HashMap<String,Integer> W2ID = new HashMap<String,Integer>();
	public HashMap<WordPair,Double> SIMMAP = new HashMap<WordPair,Double>();
	public HashMap<Integer,double[]> SENMAP = new HashMap<Integer,double[]>();
	public int numberOfDocuments;
	public int numberOfWords;
	public DocumentStatus[] documentList;
	
	public Documents(String wordFile,String wordMap,String simMap,String senMap) throws IOException{
		readFile(wordFile);
		System.out.println("WORD: FINISHING READING FILE...");
		readWordMap(wordMap);
		System.out.println("WORD: FINISHING READING WORDMAP...");
		readSimilarity(simMap);
		System.out.println("WORD: FINISHING READING SIMILARITY MAP...");
		readSentiment(senMap);
		System.out.println("WORD: FINSHING READING SENTIMENT MAP...");
		getDocumentList();
	}
	
	private void readFile(String fileAddr) throws IOException{
		File file = new File(fileAddr);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			docList.add(line);
		}
		this.numberOfDocuments=docList.size();
		this.documentList = new DocumentStatus[numberOfDocuments];
	}
	/*
	 * initiate the Word Dictionary
	 * */
	private void readWordMap(String wordMap) throws IOException{
		File file = new File(wordMap);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine();
			String terms[] = line.split(":");
			int wid = Integer.parseInt(terms[0].trim());
			String word = terms[1].trim();
			
			W2ID.put(word, wid);
			ID2W.put(wid, word);
		}
		this.numberOfWords=W2ID.size();
	}
	/*
	 * initiate the similarity map
	 * */
	private void readSimilarity(String simmap) throws IOException{
		File file = new File(simmap);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			String terms[]=line.split(":");
			String words = terms[0].trim();
			double score = Double.parseDouble(terms[1].trim());
			//if the similarity score is less than 0.001,deem it as 0
			if(score<0.001) continue;
			String word[] = words.split("-");
			int iid = Integer.parseInt(word[0].trim());
			int jid = Integer.parseInt(word[1].trim());
			WordPair wp = new WordPair(iid,jid);
			if(iid==jid) score=1.0;
			SIMMAP.put(wp, score);
		}
	}
	/*
	 * Initiate the sentiment set
	 * */
	private void readSentiment(String fileAddr) throws IOException{
		File file = new File(fileAddr);
		FileReader fr = new FileReader(file);
		Scanner scanner =new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			JSONObject obj = new JSONObject(line);
			String word = obj.getString("word").trim();
			double[] sentiments = new double[3];
			sentiments[0] = obj.getDouble("positive");
			sentiments[1] = obj.getDouble("negative");
			sentiments[2] = obj.getDouble("neutral");
		
			if(W2ID.containsKey(word)){
				//Only care about the words in the document collection
				int id = W2ID.get(word);
				SENMAP.put(id, sentiments);
			}else if(W2ID.containsKey("non-"+word)){
				int id = W2ID.get("non-"+word);
				//exchange the sentiment polarity
				double temp=sentiments[0];
				sentiments[0]=sentiments[1];
				sentiments[1]=temp;
				SENMAP.put(id, sentiments);
			}
		}
	}
	
	/*
	 * Turn docList into a list of documents to be used
	 * */
	
	private void getDocumentList(){
		for(int d=0;d<docList.size();d++){
			String docLine = docList.get(d).trim();
			docLine = docLine.replaceAll("( )+", " ").trim();// if there are many spaces, replace to one
			
			String words[]=docLine.split(" ");
			DocumentStatus ds = new DocumentStatus(d,words.length);
			
			for(int w=0;w<words.length;w++){
				String word = words[w].trim();
				int wid = W2ID.get(word);
				WordStatus ws = new WordStatus(wid);
				if(SENMAP.containsKey(wid)){
					double[] sentiments = SENMAP.get(wid);
					ws.setPositive(sentiments[0]*50);
					ws.setNegative(sentiments[1]*50);
					ws.setNeutral(sentiments[2]*50);
				}else{
					ws.setPositive(0.025*50);
					ws.setNegative(0.025*50);
					ws.setNeutral(0.95*50);
				}
				ds.wordList[w]=ws;
			}
			//calculate the similarity between words
			for(int w=0;w<words.length;w++){
				WordStatus wws = ds.wordList[w];
				int wid = wws.getWid();
				ArrayRealVector simVector = new ArrayRealVector(ds.getDocumentLength());
				for(int v=0;v<words.length;v++){
					WordStatus vws = ds.wordList[v];
					int vid=vws.getWid();
					double sim=0.0;
					if(wid==vid) sim=1.0;
					else{
						try{
							sim= SIMMAP.get(new WordPair(wid,vid));
						}catch(NullPointerException e){
							sim=0.0;
						}
					};
					simVector.setEntry(v,sim);
				}
				ds.wordList[w].setSimVector(simVector);
			}
			documentList[d]=ds;
		}
	}
	
	public static void main(String[] args) throws IOException {
//		Documents documents = new Documents();
//		String wordmap="D:/JavaProject/Sentiment Analysis/dataset/data/wordmap.txt";
//		documents.readWordMap(wordmap);
		String dir ="D:/JavaProject/Sentiment Analysis/dataset/data/wordmodel/data/";
		String wordFile=dir+"word.txt";
		String wordMap=dir+"wordmap.txt";
		String simMap = dir+"simmap.txt";
		String senMap = dir+"sentiment.txt";
		Documents documents = new Documents(wordFile,wordMap,simMap,senMap);
		//DocumentStatus[] documentList = documents.documentList;
		
	}

}
