package function;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class HashMapWriter {
	
	private String fileAddr;
	private Map map;
	
	public HashMapWriter(String fileAddr, Map map) throws IOException{
		this.fileAddr=fileAddr;
		this.map=map;
	}
	
	public void write() throws IOException{
		File file = new File(fileAddr);
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		
		Iterator iter = map.entrySet().iterator();
		while(iter.hasNext()){
			Entry entry = (Entry)iter.next();
			Object key = entry.getKey();
			Object value = entry.getValue();
			
			bw.write(key.toString()+":"+value.toString());
			bw.newLine();
			bw.flush();
		}
		bw.flush();
		bw.close();
		
	}
	

}
