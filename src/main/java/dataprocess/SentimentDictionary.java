package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.JSONObject;

public class SentimentDictionary {
	
	public void fileProcess(String input, String output) throws IOException{
		File inf = new File(input);
		FileReader fr = new FileReader(inf);
		Scanner scanner = new Scanner(fr);
		
		File outf = new File(output);
		FileWriter fw = new FileWriter(outf);
		BufferedWriter bw = new BufferedWriter(fw);
		
		while(scanner.hasNext()){
			String line = scanner.nextLine();
			String terms[]=line.split(" ");
			String type = terms[0].substring(terms[0].indexOf("=")+1).trim();
			String word = terms[2].substring(terms[2].indexOf("=")+1).trim();
			String s = terms[5].substring(terms[5].indexOf("=")+1).trim();
			if(word.contains("-")) continue;
			double positive=0.0;
			double negative=0.0;
			double neutral =0.0;
			if(s.equals("positive")){
				if(type.equals("strongsubj")){
					positive=0.8;
					negative=neutral = 0.1;
				}else{
					// the sentiment is weak
					positive= 0.6;
					neutral = 0.3;
					negative =0.1;
				}
			}else if(s.equals("negative")){
				if(type.equals("strongsubj")){
					negative=0.8;
					positive=neutral=0.1;
				}else{
					// the sentiment is weak
					negative = 0.6;
					neutral =0.3;
					positive =0.1;
				}
			}else{
				if(type.equals("strongsubj")){
					neutral =0.8;
					positive = negative =0.1;
				}else{
					neutral =0.6;
					positive = negative = 0.2;
				}
			}
			JSONObject obj = new JSONObject();
			obj.put("word", word);
			obj.put("positive", positive);
			obj.put("negative", negative);
			obj.put("neutral", neutral);
			
			bw.write(obj.toString());
			bw.newLine();
			bw.flush();
		}
		bw.flush();
		bw.close();
	}
	
	
	public static void main(String args[]) throws IOException{
		String input="D:/JavaProject/Sentiment Analysis/dataset/data/MPQA.txt";
		String output="D:/JavaProject/Sentiment Analysis/dataset/data/sentiment.txt";
		
		SentimentDictionary sd = new SentimentDictionary();
		sd.fileProcess(input, output);
	}
	

}
