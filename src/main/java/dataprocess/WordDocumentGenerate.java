/**
 * 
 */
package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 * 
 * This class is used to transfer the original raw data into a well-structured document
 * (1) remove the stop word (stop word list is a compact one, and will not filter some sentiment words out)
 * (2) remove the punctuation
 * (3) generate a word dictionary
 */
public class WordDocumentGenerate {

	/**
	 * @docList: the list of the documents
	 */
	
	List<String> docList = new ArrayList<String>();
	Set<String> stopSet = new HashSet<String>();
	Set<String> sentimentSet = new HashSet<String>();
	
	/*
	 * Read the documents into memory
	 * */
	private void readFiles(String fileAddress) throws IOException{
		File file = new File(fileAddress);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			docList.add(line);
		}
	}
	
	/*
	 * Read stop word
	 * */
	private void readStopWords(String fileAddress) throws IOException{
		File file = new File(fileAddress);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			stopSet.add(line);
		}
	}
	/*
	 * Read Sentiment words
	 * */
	private void readSentimentWords(String fileAddress) throws IOException{
		File file = new File(fileAddress);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			sentimentSet.add(line);
		}
	}
	
	/*
	 * solve the negation problem
	 * */
	private void negationProcess(){
		//for the words like not, never, neither,none, doesn't didn't haven't hadn't isn't aren't no nobody, nonsense
		for(int i=0;i<docList.size();i++){
			String doc = docList.get(i).trim();
			String newDoc="";
			doc=doc.toLowerCase();
			doc=doc.replace("don't", "do not");
			doc=doc.replace("doesn't", "does not");
			doc=doc.replace("didn't", "did not");
			doc=doc.replace("haven't", "have not");
			doc=doc.replace("isn't", "is not");
			doc=doc.replace("wasn't", "was not");
			doc=doc.replace("aren't", "are not");
			doc=doc.replace("won't", "will not");
			doc=doc.replace("wouldn't", "would not");
			doc=doc.replace("never", "not");
			doc=doc.replace("neither","not");
			doc=doc.replace("nor", "not");
			doc=doc.replace("none","not");
			doc=doc.replace(" no ", " not ");
			doc=doc.replace("I'm", "I am");
			doc=doc.replace("you're", "you are");
			doc=doc.replace("they're", "they are");
			doc=doc.replace("it's", "it is");
			
			String terms[]=  doc.split("\\pP");
			for(int t=0;t<terms.length;t++){
				if((terms[t].contains(" not "))) {
					//this sentence contains negation, all the sentiment words get a non-
					String words[]=terms[t].split(" ");
					String newLine ="";
					for(int w=0;w<words.length;w++){
						String word = words[w].trim();
						if(word.isEmpty()) continue;
						if(sentimentSet.contains(word)){
							word="non-"+word;
							newLine+=word+" ";
						}else{
							newLine+=word+" ";
						}
					}
					terms[t]=newLine;
				}
				newDoc+=terms[t].toString()+" ";
			}
			docList.set(i, newDoc);
		}
	}
	/*
	 * Remove stop word and punctuation
	 * */
	
	private void removeStopword(){
		for(int i=0;i<docList.size();i++){
			String line= docList.get(i);
			String newLine="";
			String terms[]=line.split(" ");
			for(int t=0;t<terms.length;t++){
				String term = terms[t].trim();
				if(term.isEmpty()) continue;
				term=term.replaceAll("[0-9$~]", "");
				if(term.isEmpty())continue;
				if(stopSet.contains(term)) continue;
				if(term.length()==1) continue;
				newLine+=term+" ";
			}
			newLine=newLine.trim();
			docList.set(i, newLine);
		}
	}
	
	private void writeFile(String fileName) throws IOException{
		File file = new File(fileName);
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		for(int i=0;i<docList.size();i++){
			bw.write(docList.get(i)+"\n");
			bw.flush();
		}
		bw.flush();
		bw.close();
		
	}
	
	public static void main(String[] args) throws IOException {
		String file="D:/JavaProject/Sentiment Analysis/SentimentAnalysis/data/rawdata.txt";
		String stopword = "D:/JavaProject/Sentiment Analysis/SentimentAnalysis/data/stopword.txt";
		String sentiment ="D:/JavaProject/Sentiment Analysis/SentimentAnalysis/data/sentiment.txt";
		String outputFile="D:/JavaProject/Sentiment Analysis/SentimentAnalysis/data/word.txt";
		WordDocumentGenerate wdg = new WordDocumentGenerate();
		wdg.readFiles(file);
		wdg.readStopWords(stopword);
		wdg.readSentimentWords(sentiment);
		wdg.negationProcess();
		wdg.removeStopword();
		wdg.writeFile(outputFile);
	}

}
