/**
 * 
 */
package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;

import entity.WordPair;
import function.HashMapWriter;

/**
 * @author Wanying
 * @email: wd78@drexel.edu
 * This class is used to generate the wordmap, word co-occurence dictionary
 *
 */
public class WordMapGeneration {

	/**
	 * @param args
	 */
	public HashMap<Integer,String>ID2W = new HashMap<Integer,String>();
	public HashMap<String,Integer>W2ID = new HashMap<String,Integer>();
	
	public HashMap<Integer,Integer>WordCountMap = new HashMap<Integer,Integer>();
	public HashMap<WordPair,Double> WordSimilarity = new HashMap<WordPair,Double>();
	
	private ArrayList<String> docList= new ArrayList<String>();
	
	public WordMapGeneration(String input, String wordmap,String countmap,String simmap) throws IOException{
		readFile(input);
		System.out.println("FINISHING READ FILE...");
		getWordCount(wordmap, countmap);
		System.out.println("FINISHING COUNT WORD...");
		wordSimilarity(simmap);
		System.out.println("FINISHING MEASURE SIMILARITY...");
		writeFiles(wordmap,countmap,simmap);
	}
	
	private void readFile(String wordAddr) throws IOException{
		File file = new File(wordAddr);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine();
			docList.add(line);
		}
	}
	
	/*
	 * Generate wordmap and word count
	 * */
	private void getWordCount(String wordmap, String countmap) throws IOException{
		int wid=0;
		for(int d=0;d<docList.size();d++){
			String line = docList.get(d).trim();
			String words[] = line.split(" ");
			for(int w=0;w<words.length;w++){
				String word = words[w].trim();
				if(word.isEmpty()) continue;
				if(W2ID.containsKey(word)){
					int id = W2ID.get(word);
					int count = WordCountMap.get(id);
					count=count+1;
					WordCountMap.put(id, count);
				}else{
					W2ID.put(word, wid);
					ID2W.put(wid,word);
					WordCountMap.put(wid, 1);
					wid++;
				}
			}
		}
		HashMapWriter wordWriter = new HashMapWriter(wordmap,ID2W);
		wordWriter.write();
		HashMapWriter countWriter = new HashMapWriter(countmap,WordCountMap);
		countWriter.write();
	}
	
	/*
	 * Generate Word Similarity Map
	 * Method:
	 * 	We use Exponential Decay to calculate the similarity
	 *  x = exp(-d), d-->[1,document length]
	 *  sim = (x-exp(-len))/(exp(-1)-exp(-len))
	 * */
	
	private void wordSimilarity(String simmap) throws IOException{
		double[][]simSum=new double[W2ID.size()][W2ID.size()];
		
		for(int d=0;d<docList.size();d++){
			String line=docList.get(d).trim();
			String words[]=line.split(" ");
			int ids[]=new int[words.length];
			for(int w=0;w<words.length;w++){
				String word = words[w].trim();
				if(word.isEmpty()) continue;
				int wid = W2ID.get(word);
				ids[w]=wid;
			}
			double len = words.length;
			for(int i=0;i<words.length;i++){
				for(int j=0;j<words.length;j++){
					if(i==j)continue;
					int iid = ids[i];
					int jid = ids[j];
					double s=Math.exp(-(Math.abs(j-i)));
					double sim=(s-Math.exp(-len))/(Math.exp(-1)-Math.exp(-len));
					simSum[iid][jid]=sim;
				}
			}
		}
		
		for(int i=0;i<W2ID.size()-1;i++){
			double iMax=0.0;
			double iMin=1.0;
			for(int j=i+1;j<W2ID.size();j++){
				if(simSum[i][j]>iMax) iMax=simSum[i][j];
				if(simSum[i][j]<iMin) iMin=simSum[i][j];
			}
			if(iMax==0) continue;
			if(iMax==iMin) continue;
			for(int j=i+1;j<W2ID.size();j++){
				double ijs=(simSum[i][j]-iMin)/(iMax-iMin);
				if(ijs==0)continue;
				WordPair wp = new WordPair(i,j);
				WordSimilarity.put(wp, ijs);
			}
			//System.out.println("FINISHING PROCESSING WORD: "+i+"/"+W2ID.size()+"...");
		}
	}
	
	private void writeFiles(String wordMap, String countMap, String simMap) throws IOException{
		HashMapWriter wordWriter = new HashMapWriter(wordMap,ID2W);
		HashMapWriter countWriter = new HashMapWriter(countMap,WordCountMap);
		HashMapWriter simWriter = new HashMapWriter(simMap,WordSimilarity);
		
		wordWriter.write();
		countWriter.write();
		simWriter.write();
	}
	
	
	public static void main(String[] args) throws IOException {
		String dir="D:/JavaProject/Sentiment Analysis/dataset/data/wordmodel/data/";
		String input = dir+"word.txt";
		String wordmap = dir+"wordmap.txt";
		String countmap =dir+"countmap.txt";
		String simmap = dir+"simmap.txt";
		
		WordMapGeneration wc = new WordMapGeneration(input,wordmap,countmap,simmap);

	}

}
