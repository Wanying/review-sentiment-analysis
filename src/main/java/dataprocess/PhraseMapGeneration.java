/**
 * 
 */
package dataprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;

import entity.WordPair;
import function.HashMapWriter;


/**
 * @author Wanying
 * @email: wd78@drexel.edu
 */
public class PhraseMapGeneration {

	/**
	 * This class is used to process the phrase document into an ideal status to read
	 * @throws IOException 
	 */
	private List<String> docList = new ArrayList<String>();
	private List<String> documentList = new ArrayList<String>();
	public HashMap<String, Integer> H2ID = new HashMap<String,Integer>();
	public HashMap<Integer,String> ID2H = new HashMap<Integer,String>();
	public HashMap<String,Integer> M2ID = new HashMap<String,Integer>();
	public HashMap<Integer,String> ID2M = new HashMap<Integer,String>();
	
	public HashMap<Integer,Integer> HCOUNT = new HashMap<Integer,Integer>();
	public HashMap<Integer,Integer> MCOUNT = new HashMap<Integer,Integer>();
	public HashSet<String> STOPSET = new HashSet<String>();
	public HashMap<WordPair,Double> SIMMAP = new HashMap<WordPair,Double>();
	
	
	public PhraseMapGeneration(String stopFile,String rawFile,String refineFile,String headMap,String modifierMap,String headCount,String modiCount,String headSimFile) throws IOException{
		readStopSet(stopFile);
		System.out.println("FINISHING READING STOP FILE...");
		refineFile(rawFile,refineFile);
		System.out.println("FINISHING REFINING FILE...");
		writeWordMap(headMap,modifierMap,headCount,modiCount);
		System.out.println("FINISHING WRITING MAPS...");
		writeSimilarityMap(headSimFile);	
		System.out.println("FINISHING WRITING SIMILARITY FILE...");
	}
	private void readStopSet(String stopFile) throws FileNotFoundException{
		File file = new File(stopFile);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			STOPSET.add(line);
		}
	}
	
	private void refineFile(String phraseFileAddress,String outputFile) throws IOException{
		File file = new File(phraseFileAddress);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			String newline="";
			line=line.replaceAll("&\\d+&\\d", "");
			line=line.replace("/", "");
			String ps[]= line.split(";");
			for(int p=0;p<ps.length;p++){
				String phrase = ps[p].trim();
				if(phrase.isEmpty()) continue;
				
				String terms[]= phrase.split(",");
				String head = terms[0].trim();
				String modifier = terms[1].trim();
				
				head=head.replaceAll("[?&%$!.~+:=@#<>]", "").trim();
				head=head.replace("\\", "").trim();
				modifier=modifier.replaceAll("[?&%$!.~+:=@#<>]", "").trim();
				modifier=modifier.replace("\\", "").trim();
				if((head.isEmpty())||(modifier.isEmpty())) continue;
				if(STOPSET.contains(head)||STOPSET.contains(modifier)) continue;
				newline+=head+","+modifier+";";
			}
			if(newline.trim().isEmpty())continue;
			if(newline.split(";").length==1)continue;
			documentList.add(newline);
		}
		listWrite(documentList,outputFile);
	}
	
	private void writeWordMap(String headMap,String modiMap,String headCount,String modiCount) throws IOException{
		
		int hid=0;
		int mid=0;
		
		for(int d=0;d<documentList.size();d++){
			String doc = documentList.get(d).trim();
			String ps[] = doc.split(";");
			for(int p=0;p<ps.length;p++){
				String phrase = ps[p].trim();
				if(phrase.isEmpty())continue;
				String items[]=phrase.split(",");
				String head = items[0].trim();
				String modifier = items[1].trim();
				
				if(!(H2ID.containsKey(head))){
					H2ID.put(head,hid);
					ID2H.put(hid, head);
					HCOUNT.put(hid, 1);
					hid++;
				}else{
					int id = H2ID.get(head);
					int count = HCOUNT.get(id);
					count=count+1;
					HCOUNT.put(id, count);
				}
				if(!(M2ID.containsKey(modifier))){
					M2ID.put(modifier, mid);
					ID2M.put(mid, modifier);
					MCOUNT.put(mid, 1);
					mid++;
				}else{
					int id=M2ID.get(modifier);
					int count = MCOUNT.get(id);
					count=count+1;
					MCOUNT.put(id, count);
				}
			}
		}
		HashMapWriter hMapWriter = new HashMapWriter(headMap,H2ID);
		HashMapWriter mMapWriter = new HashMapWriter(modiMap,M2ID);
		HashMapWriter hCountWriter = new HashMapWriter(headCount,HCOUNT);
		HashMapWriter mCountWriter = new HashMapWriter(modiCount,MCOUNT);
		hMapWriter.write();
		mMapWriter.write();
		hCountWriter.write();
		mCountWriter.write();
		
	}
	
	private void writeSimilarityMap(String headSimFile) throws IOException{
		
		double[][] simSum=new double[H2ID.size()][H2ID.size()];
		
		for(int d=0;d<documentList.size();d++){
			String line = documentList.get(d).trim();
			String phrases[] = line.split(";");
			int hids[] = new int[phrases.length];
			int len = hids.length;//consider both head and modifier words
			for(int p=0;p<phrases.length;p++){
				String phrase=phrases[p].trim();
				String words[] = phrase.split(",");
				String head = words[0].trim();
				if(head.isEmpty()) continue;
				int hid = H2ID.get(head);
				hids[p]=hid;
			}
			
			for(int i=0;i<phrases.length;i++){
				for(int j=0;j<phrases.length;j++){
					if(i==j)continue;
					int iid = hids[i];
					int jid = hids[j];
					double s = Math.exp(-(Math.abs(j-i)));
					double sim=(s-Math.exp(-len))/(Math.exp(-1)-Math.exp(-len));
					simSum[iid][jid]+=sim/(double)(HCOUNT.get(iid)+HCOUNT.get(jid));
				}
			}
		}
		
		for(int i=0;i<H2ID.size()-1;i++){
			double iMax=0.0;
			double iMin=1.0;
			for(int j=i+1;j<H2ID.size();j++){
				//System.out.println("Similarity: "+simSum[i][j]+";"+simSum[j][i]);
				if(simSum[i][j]>iMax) iMax=simSum[i][j];
				if(simSum[i][j]<iMin) iMin=simSum[i][j];
			}
			if(iMax==0.0) continue;
			if(iMax==iMin) continue;
			for(int j=i+1;j<H2ID.size();j++){
				double ijs = (simSum[i][j]-iMin)/(iMax-iMin);
				if(ijs==0) continue;
				WordPair wp = new WordPair(i,j);
				SIMMAP.put(wp, ijs);
			}
			//System.out.println("FINISHING WORD "+i+"/"+H2ID.size()+"...");
		}
		HashMapWriter writer = new HashMapWriter(headSimFile,SIMMAP);
		writer.write();
	}
	
	
	private void listWrite(List list, String fileName) throws IOException{
		File file = new File(fileName);
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		for(int i=0;i<list.size();i++){
			String line = list.get(i).toString();
			bw.write(line);
			bw.newLine();
			bw.flush();
		}
		bw.flush();
		bw.close();
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String dir="D:/JavaProject/Sentiment Analysis/dataset/data/";
		String stopFile=dir+"stopword.txt";
		String rawFile=dir+"/phrasemodel/raw.txt";
		
		String gdir=dir+"/phrasemodel/data/";
		String refineFile=gdir+"phrase.txt";
		String headMap =gdir+"headMap.txt";
		String modifierMap=gdir+"modifierMap.txt";
		String headCount=gdir+"headCount.txt";
		String modiCount=gdir+"modiCount.txt";
		String headSimFile=gdir+"headSimilarity.txt";
		//stopFile,rawFile,refineFile,headMap,modifierMap,headSimFile
		PhraseMapGeneration pmg = new PhraseMapGeneration(stopFile,rawFile,refineFile,headMap,modifierMap,headCount,modiCount,headSimFile);

	}

}
