/**
 * 
 */
package phrasemodel;

import org.apache.commons.math3.linear.ArrayRealVector;

/**
 * @author Wanying
 * @email wd78@drexel.edu
 *
 */
public class TableStatus {

	/**
	 * @tid table id
	 * @did to which document this table belongs to
	 * @phraseVector: the phrases in this table, the Integer here represents the position of the phrase
	 * @topicAssignment: to which topic, this table has been assigned to
	 */
	
	private int tid;
	private int did;
	public ArrayRealVector headWordVector;
	public int topicAssignment;
	public int numberOfHeadWord;
	
	public TableStatus(int tid, int did, int documentLength){
		this.tid=tid;
		this.did=did;
		this.headWordVector=new ArrayRealVector(documentLength);
		this.topicAssignment=-1;
		this.numberOfHeadWord=0;
	}
	//a short version of table status
	public TableStatus(int tid, int did){
		this.tid=tid;
		this.did=did;
	}
	
	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}
	
	public boolean equals(Object obj){
		TableStatus ts =(TableStatus)obj;
		if((ts.tid==this.tid)&&(ts.did==this.did))return true;
		else return false;
	}
	
	public int hashCode(){
		int result=31;
		result+=this.tid*17;
		result+=this.did*17;
		return result;
	}



	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
