package phrasemodel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import entity.WordPair;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @version Sep 24, 2014
 *
 */
public class Model {

	/**
	 * This Class is used to initiate the model for the whole process
	 */
	
	public int numberOfDocument;
	public int numberOfHeadWord;
	public int numberOfModiWord;
	public int numberOfTopic;
	public int numberOfUpperLimitTopic;
	public int numberOfSentiment;
	
	public double alpha;
	public double beta;
	public double gamma;
	public double delta[];
	public double epsilon;
	
	public HashMap<Integer,String>ID2H;
	public HashMap<String,Integer>H2ID;
	public HashMap<Integer,String>ID2M;
	public HashMap<String,Integer>M2ID;
	public HashMap<WordPair,Double> SIMMAP;
	public HashMap<Integer,double[]> SENMAP;
	
	public DocumentStatus[] documentList;
	public List<TopicStatus> topicList;
	public int iter;
	
	private Random random = new Random();
	
	public Model(Documents documents, int initK, int upperK, int initS){
		readDocuments(documents);
		initModel(initK,upperK, initS);
	}
	
	private void readDocuments(Documents documents){
		this.numberOfDocument=documents.numberOfDocuments;
		this.numberOfHeadWord=documents.numberOfHeadWords;
		this.numberOfModiWord=documents.numberOfModiWords;
		this.ID2H=documents.ID2H;
		this.H2ID=documents.H2ID;
		this.ID2M=documents.ID2M;
		this.M2ID=documents.M2ID;
		this.SIMMAP=documents.SIMMAP;
		this.SENMAP=documents.SENMAP;
		
		this.documentList=documents.documentList;
	}
	
	private void initModel(int initK,int upperK, int initS){
		/*
		 * Randomly assign words to different topic and sentiment aloocaiton
		 * Given an upperK to numberOfTopic, and this value will change 
		 * Given an initS(3) to numberOfSentiment, and this value will not change
		 * */
		this.numberOfTopic=initK;
		this.numberOfUpperLimitTopic=upperK;
		this.numberOfSentiment=initS;
		this.topicList = new ArrayList<TopicStatus>();
		
		this.alpha=1E-8;;
		this.beta=0.05;
		this.gamma=1E-8;
		this.delta = new double[numberOfSentiment];
		this.epsilon=0.001;
		this.iter=1000;
		
		/*
		 * Initiate topic Assignment
		 * */
		for(int t=0;t<numberOfTopic;t++){
			TopicStatus ts = new TopicStatus(t,numberOfHeadWord, numberOfModiWord,numberOfSentiment);
			topicList.add(ts);
		}
		
		for(int d=0;d<documentList.length;d++){
			int length= documentList[d].getDocumentLength();
			for(int p=0;p<length;p++){
				PhraseStatus ps = documentList[d].phraseList[p];
				int hid = ps.getHid();
				int mid = ps.getMid();
				
				int tableAssignment = documentList[d].numberOfTable;
				int topicAssignment = random.nextInt(numberOfTopic);
				int sentimentAssignment = random.nextInt(numberOfSentiment);
				
				//phrase level
				documentList[d].phraseList[p].tableAssignment=tableAssignment;
				documentList[d].phraseList[p].topicAssignment=topicAssignment;
				documentList[d].phraseList[p].sentimentAssignment=sentimentAssignment;
				
				//table level
				TableStatus ts = new TableStatus(tableAssignment,d,documentList[d].getDocumentLength());
				ts.numberOfHeadWord++;
				ts.headWordVector.setEntry(p, 1);
				ts.topicAssignment=topicAssignment;
				documentList[d].addTable(ts);
				documentList[d].numberOfTable++;
				//topic level
				topicList.get(topicAssignment).numberOfHeadWord++;
				topicList.get(topicAssignment).headWordList[hid]++;
				//sentiment level
				topicList.get(topicAssignment).numberOfModiWord++;
				topicList.get(topicAssignment).tsw[sentimentAssignment][mid]++;
				topicList.get(topicAssignment).sw[sentimentAssignment]++;
			}
		}
	}

	public static void main(String[] args) throws IOException {
		String dir="D:/JavaProject/Sentiment Analysis/dataset/data/phrasemodel/data/";
		String fileAddr=dir+"phrase.txt";
		String headMap = dir+"headMap.txt";
		String modiMap = dir+"modifierMap.txt";
		String simFile = dir+"headSimilarity.txt";
		
		String sDir="D:/JavaProject/Sentiment Analysis/dataset/data/";
		String sentFile = sDir+"sentiment.txt";
		
		Documents documents = new Documents(fileAddr,headMap,modiMap,simFile,sentFile);
		int initK=10;
		int initS=3;
		int upperK=100;
		Model model = new Model(documents,initK,upperK,initS);
		
		DocumentStatus documentList[] = documents.documentList;
		for(int d=0;d<documentList.length;d++){
			System.out.println(documentList[d].toString());
		}
		
		
	}

}
