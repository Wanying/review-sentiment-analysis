package phrasemodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.json.JSONObject;

import entity.WordPair;

/**
 * @author Wanying
 * @email wd78@drexel.edu
 * */
public class Documents {
	
	public List<String> docList = new ArrayList<String>();
	public HashMap<Integer,String>ID2H = new HashMap<Integer,String>();
	public HashMap<String,Integer>H2ID = new HashMap<String,Integer>();
	public HashMap<Integer,String>ID2M = new HashMap<Integer,String>();
	public HashMap<String,Integer>M2ID = new HashMap<String,Integer>();
	
	public HashMap<Integer,double[]>SENMAP = new HashMap<Integer,double[]>();
	public HashMap<WordPair,Double> SIMMAP = new HashMap<WordPair,Double>();
	
	public DocumentStatus[] documentList;
	
	public int numberOfDocuments;
	public int numberOfHeadWords;
	public int numberOfModiWords;
	
	public Documents(String fileAddr,String headMap,String modiMap,String simFile,String sentFile) throws IOException{
		readDataFile(fileAddr);
		System.out.println("PHRASE: FINISHING READING FILES...");
		readWordMap(headMap,modiMap);
		System.out.println("PHRASE: FINISHING READING MAPS...");
		readSimilarity(simFile);
		System.out.println("PHRASE: FINISHING READING SIMILARITY...");
		readSentiment(sentFile);
		System.out.println("PHRASE: FINISHING READING SENTIMENT...");
		getDocumentList();
	}
	
	private void readDataFile(String fileAddr) throws IOException{
		File file = new File(fileAddr);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			docList.add(line);
		}
		this.numberOfDocuments=docList.size();
		documentList = new DocumentStatus[this.numberOfDocuments];
	}
	
	private void readWordMap(String headFileAddr,String modiFileAddr) throws IOException{
		File headFile = new File(headFileAddr);
		FileReader headReader = new FileReader(headFile);
		Scanner headScanner = new Scanner(headReader);
		while(headScanner.hasNext()){
			String line = headScanner.nextLine().trim();
			String terms[]=line.split(":");
			String word=terms[0].trim();
			int id=Integer.parseInt(terms[1].trim());
			ID2H.put(id, word);
			H2ID.put(word, id);
		}
		this.numberOfHeadWords=H2ID.size();
		
		File modiFile = new File(modiFileAddr);
		FileReader modiReader = new FileReader(modiFile);
		Scanner modiScanner = new Scanner(modiReader);
		while(modiScanner.hasNext()){
			String line = modiScanner.nextLine().trim();
			String terms[] = line.split(":");
			String word= terms[0].trim();
			int id = Integer.parseInt(terms[1].trim());
			ID2M.put(id, word);
			M2ID.put(word, id);
		}
		this.numberOfModiWords=M2ID.size();
	}
	
	private void readSimilarity(String simFileAddr) throws IOException{
		File file = new File(simFileAddr);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			String terms[]= line.split(":");
			String wordstr = terms[0].trim();
			Double sim = Double.parseDouble(terms[1].trim());
			if(sim<0.01) continue;
			String words[] = wordstr.split("-");
			int iid = Integer.parseInt(words[0].trim());
			int jid = Integer.parseInt(words[1].trim());
			WordPair wp = new WordPair(iid,jid);
			SIMMAP.put(wp, sim);
		}
	}
	
	private void readSentiment(String sentFileAddr) throws IOException{
		File file = new File(sentFileAddr);
		FileReader fr = new FileReader(file);
		Scanner scanner = new Scanner(fr);
		
		while(scanner.hasNext()){
			String line = scanner.nextLine().trim();
			JSONObject jsonObj = new JSONObject(line);
			String word = jsonObj.getString("word").trim();
			double positive=jsonObj.getDouble("positive");
			double negative=jsonObj.getDouble("negative");
			double neutral =jsonObj.getDouble("neutral");
			
			if(M2ID.containsKey(word)){
				int mid = M2ID.get(word);
				double[] sims= new double[3];
				sims[0]=positive;
				sims[1]=negative;
				sims[2]=neutral;
				SENMAP.put(mid, sims);
			}
		}
	}
	
	private void getDocumentList(){
		for(int d=0;d<docList.size();d++){
			String docLine = docList.get(d).trim();
			String phrases[]  = docLine.split(";");
			DocumentStatus ds = new DocumentStatus(d,phrases.length);
			
			for(int p=0;p<phrases.length;p++){
				String phrase = phrases[p].trim();
				String terms[]=phrase.split(",");
				String head=terms[0].trim();
				String modifier = terms[1].trim();
				
				int hid = H2ID.get(head);
				int mid = M2ID.get(modifier);
				PhraseStatus ps = new PhraseStatus(hid,mid);
				if(SENMAP.containsKey(mid)){
					double[] sentiments = SENMAP.get(mid);
					ps.setPositive(sentiments[0]*50);
					ps.setNegative(sentiments[1]*50);
					ps.setNeutral(sentiments[2]*50);
				}else{
					ps.setPositive(0.3*50);
					ps.setNegative(0.3*50);
					ps.setNeutral(0.4*50);;
				}
				ds.phraseList[p]=ps;
			}
			
			//calculate the similarity between the head words
			for(int p=0;p<phrases.length;p++){
				PhraseStatus pps = ds.phraseList[p];
				int phid = pps.getHid();
				ArrayRealVector simVector = new ArrayRealVector(ds.getDocumentLength());
				for(int s=0;s<phrases.length;s++){
					PhraseStatus sps = ds.phraseList[s];
					int shid = sps.getHid();
					double sim=0.0;
					if(phid==shid) sim=1.0;
					else{
						try{
							sim=SIMMAP.get(new WordPair(phid,shid));
						}catch(NullPointerException e){
							sim=0.0;
						}
					}
					simVector.setEntry(s, sim);
				}
				ds.phraseList[p].setSimVector(simVector);
			}
			documentList[d]=ds;
		}
	}
	//for test
	public static void main(String args[]) throws IOException{
		String dir="D:/JavaProject/Sentiment Analysis/dataset/data/phrasemodel/data/";
		String fileAddr=dir+"phrase.txt";
		String headMap = dir+"headMap.txt";
		String modiMap = dir+"modifierMap.txt";
		String simFile = dir+"headSimilarity.txt";
		
		String sDir="D:/JavaProject/Sentiment Analysis/dataset/data/";
		String sentFile = sDir+"sentiment.txt";
		
		Documents documents = new Documents(fileAddr,headMap,modiMap,simFile,sentFile);
		DocumentStatus ds[]=documents.documentList;
		
		for(int d=0;d<ds.length;d++){
			System.out.println(ds[d].toString());
		}
		
	}
}
