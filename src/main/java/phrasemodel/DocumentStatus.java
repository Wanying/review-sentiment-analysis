package phrasemodel;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Wanying
 * @email wd78@drexel.edu
 * */

public class DocumentStatus {
	private int did;
	public PhraseStatus[] phraseList;
	private int documentLength;
	public int numberOfTable;
	public List<TableStatus> tableList;
	
	public DocumentStatus(int id,int documentLength){
		this.did=id;
		this.documentLength=documentLength;
		this.numberOfTable=0;
		this.phraseList=new PhraseStatus[documentLength];
		this.tableList = new ArrayList<TableStatus>();
	}

	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}

	public int getDocumentLength() {
		return documentLength;
	}

	public void setDocumentLength(int documentLength) {
		this.documentLength = documentLength;
	}
	
	
	public String toString(){
		String str=did+","+documentLength+","+numberOfTable+":";
		for(int p=0;p<phraseList.length;p++){
			str+=phraseList[p].toString()+"\t";
		}
		return str;
	}
	
	public void addTable(TableStatus ts){
		this.tableList.add(ts);
	}
	public void removeTable(TableStatus ts){
		int tid = ts.getTid();
		this.tableList.remove(ts);
		for(int t=0;t<tableList.size();t++){
			int id = tableList.get(t).getTid();
			if(id>tid){
				this.tableList.get(t).setTid(id-1);
			}
		}
		for(int p=0;p<this.phraseList.length;p++){
			if(this.phraseList[p].tableAssignment>tid){
				this.phraseList[p].tableAssignment--;
			}
		}
		this.numberOfTable--;
	}

}
