package phrasemodel;

import java.io.IOException;
/**
 * 
 */

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @version Sep 25, 2014
 *
 */
public class PhraseModel {

	/**
	 * @param args
	 */
	private String dataDir;
	private String resultDir;
	private String commonDir;
	
	public PhraseModel(String commonDir, String dataDir, String resultDir){
		this.commonDir=commonDir;
		this.dataDir = dataDir;
		this.resultDir=resultDir;
	}
	
	public void run(int initK,int upperK, int initS) throws IOException{
		String sentFile=commonDir+"sentiment.txt";
		String fileAddr = dataDir+"phrase.txt";
		String headMap = dataDir+"headMap.txt";
		String modiMap = dataDir+"modifierMap.txt";
		String simFile = dataDir+"headSimilarity.txt";
		
		Documents documents = new Documents(fileAddr,headMap,modiMap,simFile,sentFile);
		Model model = new Model(documents,initK,upperK,initS);
		GibbsSampling gs = new GibbsSampling(model);
		
		String confFile=resultDir+"confFile.txt";
		String topicList = resultDir+"topicList.txt";
		String topicDir=resultDir+"topicDir";
		String documentFile=resultDir+"documentFile.txt";
		String topicFile = resultDir+"topicFile.txt";
		
		ModelSave ms = new ModelSave(gs);
		ms.saveConfigurationFile(confFile);
		ms.saveTopicList(topicList);
		ms.saveTopicFiles(topicDir, topicFile);
		ms.saveDocumentFiles(documentFile);
		
		Perplexity ppx = new Perplexity(gs);
		double perplexity = ppx.getPerplexity(confFile);
		System.out.println("Perplexity of Phrase Model is: "+perplexity);	
		
	}
	public static void main(String[] args) throws IOException {
		String commonDir = "D:/JavaProject/Sentiment Analysis/dataset/data/";
		String dataDir="D:/JavaProject/Sentiment Analysis/dataset/data/phrasemodel/data/";
		String resultDir="D:/JavaProject/Sentiment Analysis/dataset/data/phrasemodel/result/";
		
		PhraseModel pm = new PhraseModel(commonDir,dataDir,resultDir);
		pm.run(10,100,3);

	}

}
