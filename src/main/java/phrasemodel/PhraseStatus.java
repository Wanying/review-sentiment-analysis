/**
 * 
 */
package phrasemodel;

import org.apache.commons.math3.linear.ArrayRealVector;

/**
 * @author Wanying
 * @email wd78@drexel.edu
 *
 */
public class PhraseStatus {

	/**
	 * @param args
	 */
	private int hid;
	private int mid;
	public int tableAssignment;
	public int topicAssignment;
	public int sentimentAssignment;
	private double positive;
	private double negative;
	private double neutral;
	
	private ArrayRealVector simVector;
	
	public PhraseStatus(int hid, int mid){
		this.hid=hid;
		this.mid=mid;
		this.tableAssignment=-1;
		this.topicAssignment=-1;
		this.sentimentAssignment=-1;
		this.positive=-1;
		this.negative=-1;
		this.neutral=-1;
	}
	
	public int getHid() {
		return hid;
	}

	public void setHid(int hid) {
		this.hid = hid;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public ArrayRealVector getSimVector() {
		return simVector;
	}

	public void setSimVector(ArrayRealVector simVector) {
		this.simVector = simVector;
	}
	
	
	public double getPositive() {
		return positive;
	}

	public void setPositive(double positive) {
		this.positive = positive;
	}

	public double getNegative() {
		return negative;
	}

	public void setNegative(double negative) {
		this.negative = negative;
	}

	public double getNeutral() {
		return neutral;
	}

	public void setNeutral(double neutral) {
		this.neutral = neutral;
	}

	public boolean equals(Object obj){
		PhraseStatus ps = (PhraseStatus)obj;
		if((ps.hid==this.hid)&&(ps.mid==this.mid))return true;
		else return false;
	}
	
	public int hashCode(){
		int result=31;
		result+=hid*17;
		result+=mid*17;
		return result;
	}
	
	public String toString(){
		return this.hid+"&"+this.mid+"&"+this.tableAssignment+"&"+this.topicAssignment+"&"+this.sentimentAssignment;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
