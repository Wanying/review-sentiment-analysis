package phrasemodel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
/**
 * 
 */

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @version Sep 25, 2014
 *
 */
public class Perplexity {

	/**
	 * This class is used to calculate the Perplexity of this model
	 * p(p)=p(w)*p(s)
	 * p(p)=(p(w|z)*p(z|d))*(p(s|z)*p(z|d)))
	 */
	
	private GibbsSampling gs;
	public Perplexity(GibbsSampling gs){
		this.gs=gs;
	}
	
	public double getPerplexity(String confFile) throws IOException{
		double perplexity =-1;
		
		double loglik=0;
		double zeta=0.01;
		List<TopicStatus> topicList = gs.topicList;
		
		int sumOfWords=0;
		for(int d=0;d<gs.numberOfDocument;d++){
			DocumentStatus ds = gs.documentList[d];
			sumOfWords+=ds.getDocumentLength();
			sumOfWords+=ds.getDocumentLength();
			PhraseStatus[] phraseList = ds.phraseList;
			int [] dtd = new int[gs.numberOfTopic];
			for(int p=0;p<phraseList.length;p++){
				PhraseStatus ps = phraseList[p];
				int topicAssignment=ps.topicAssignment;
				dtd[topicAssignment]++;
			}
			
			for(int p=0;p<phraseList.length;p++){
				double simSum=0.0;
				PhraseStatus ps = phraseList[p];
				int hid=ps.getHid();
				int mid=ps.getMid();
				
				for(int t=0;t<gs.numberOfTopic;t++){
					//p(z|d)
					double pzd=(dtd[t]+zeta)/(ds.getDocumentLength()+gs.numberOfTopic*zeta);
					//p(w|z,d)
					double pwzd = (topicList.get(t).headWordList[hid]+gs.beta)/(topicList.get(t).numberOfHeadWord+gs.numberOfHeadWord*gs.beta);
					double deltaSum = gs.delta[0]+gs.delta[1]+gs.delta[2];
					double pwsd=0;
					for(int s=0;s<gs.numberOfSentiment;s++){
						//p(s|z)
						double psz=(topicList.get(t).sw[s]+gs.delta[s])/(topicList.get(t).numberOfModiWord+deltaSum);
						//p(w|s,z)
						double pwsz=(topicList.get(t).tsw[s][mid]+gs.epsilon)/(topicList.get(t).sw[s]+gs.numberOfModiWord*gs.epsilon);
						pwsd+=psz*pwsz;
					}
					double sim=pzd*pwzd*pwsd;
					simSum+=sim;
				}
				
				loglik+=Math.log(simSum);
			}
			
		}
		perplexity=Math.exp(-loglik/(double)sumOfWords);
		
		 perplexity=Math. exp(-loglik/( double)sumOfWords);
	       
	       File file= new File(confFile);
	       FileWriter fw = new FileWriter(file, true);
	       BufferedWriter bw = new BufferedWriter(fw);
	       bw.newLine();
	       bw.write( "Perplexity: " +perplexity);
	       bw.newLine();
	       bw.flush();
	       bw.close();
		
		
		
		return perplexity;
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
