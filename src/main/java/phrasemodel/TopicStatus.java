/**
 * 
 */
package phrasemodel;

import java.util.HashSet;

/**
 * @author Wanying
 * @email wd78@drexel.edu
 */
public class TopicStatus {

	/**
	 * This is used to record the topic status
	 */
	public int tid;
	public int numberOfHeadWord;
	public int numberOfModiWord;
	public double[] headWordList;
	public int[][] tsw;
	public int[]sw;
	public HashSet<Integer> headWordSet;
	
	public TopicStatus(int tid, int numberOfHeadWord,int numberOfModiWord, int numberOfSentiment){
		this.tid=tid;
		this.numberOfHeadWord=0;
		this.numberOfModiWord=0;
		this.headWordList=new double[numberOfHeadWord];
		this.tsw=new int[numberOfSentiment][numberOfModiWord];
		this.sw= new int[numberOfSentiment];
		this.headWordSet = new HashSet<Integer>();
	}

}
