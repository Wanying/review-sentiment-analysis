package phrasemodel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * 
 */
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;

import entity.WordPair;

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @version Sep 24, 2014
 *
 */
public class GibbsSampling {

	/**
	 * This Class is used to implement Gibbs Sampling to optimize the whole process
	 */
	
	public double alpha;
	public double beta;
	public double gamma;
	public double delta[];
	public double epsilon;
	
	public int numberOfDocument;
	public int numberOfHeadWord;
	public int numberOfModiWord;
	public int numberOfTopic;
	public int numberOfSentiment;
	public int numberOfUpperLimitTopic;
	
	public List<TopicStatus> topicList;
	public DocumentStatus[] documentList;
	
	public HashMap<String,Integer> H2ID;
	public HashMap<Integer,String> ID2H;
	public HashMap<String,Integer> M2ID;
	public HashMap<Integer,String> ID2M;
	public HashMap<WordPair,Double> SIMMAP;
	public HashMap<Integer,double[]> SENMAP;
	public List<ArrayRealVector>SIMMATRIX;
	public int iter;
	
	private Random random = new Random();
	
	public GibbsSampling(Model model){
		loadModel(model);
		run();
	}
	
	private void loadModel(Model model){
		this.numberOfDocument=model.numberOfDocument;
		this.numberOfTopic=model.numberOfTopic;
		this.numberOfUpperLimitTopic=model.numberOfUpperLimitTopic;
		this.numberOfHeadWord=model.numberOfHeadWord;
		this.numberOfModiWord=model.numberOfModiWord;
		this.numberOfSentiment=model.numberOfSentiment;
		
		this.documentList=model.documentList;
		this.topicList=model.topicList;
		
		this.alpha=model.alpha;
		this.beta=model.beta;
		this.gamma=model.gamma;
		this.delta=model.delta;
		this.epsilon=model.epsilon;
		this.iter=model.iter;
		
		this.H2ID=model.H2ID;
		this.ID2H=model.ID2H;
		this.M2ID=model.M2ID;
		this.ID2M=model.ID2M;
		this.SIMMAP=model.SIMMAP;
		this.SENMAP=model.SENMAP;
		
		this.SIMMATRIX=new ArrayList<ArrayRealVector>();
		
		for(int i=0;i<numberOfHeadWord;i++){
			ArrayRealVector vector = new ArrayRealVector(numberOfHeadWord);
			for(int j=0;j<numberOfHeadWord;j++){
				WordPair wp =new WordPair(i,j);
				if(SIMMAP.containsKey(wp)){
					vector.setEntry(j, SIMMAP.get(wp));
				}else{
					vector.setEntry(j, 0.0);
				}
			}
			SIMMATRIX.add(vector);
		}
	}
	private void run(){
		for(int i=0;i<iter;i++){
			System.out.print("PHRAE MODEL SAMPLING "+i+"/"+iter+": \t");
			sampling();
		}
	}
	
	private void sampling(){
		for(int d=0;d<numberOfDocument;d++){
			for(int p=0;p<documentList[d].phraseList.length;p++){
				//long time1 = System.nanoTime();
				decrement(d,p);
				//long time2 = System.nanoTime();
				//System.out.println("DECREMENT TIME CONSUMING: "+(time2-time1));
				int tableAssignment=tableSampling(d,p);
				//long time3 = System.nanoTime();
				//System.out.println("TABLE ASSINGMENT TIME CONSUMING: "+(time3-time2));
				int topicAssignment;
				if(tableAssignment==documentList[d].numberOfTable){
					//System.out.println("CREATING A NEW TABLE...");
					topicAssignment=topicSampling(d,p);
					if(topicAssignment==this.numberOfTopic){
						TopicStatus ts = new TopicStatus(this.numberOfTopic,numberOfHeadWord,numberOfModiWord,numberOfSentiment);
						this.topicList.add(ts);
						this.numberOfTopic++;
					}
				}else{
					topicAssignment = documentList[d].tableList.get(tableAssignment).topicAssignment;
				}
				//long time4 = System.nanoTime();
				//System.out.println("TOPIC ASSIGNMENT TIME CONSUMING: "+(time4-time3));
				int sentimentAssignment=sentimentSampling(d,p,topicAssignment);
				//long time5=System.nanoTime();
				//System.out.println("SENTIMENT ASSIGNMENT TIME CONSUMING: "+(time5-time4));
				allocate(d,p,tableAssignment,topicAssignment,sentimentAssignment);
				//long time6=System.nanoTime();
				//System.out.println("ALLOCATE TIME CONSUMING: "+(time6-time5));
			}
		}
		System.out.println("NUMBER OF TOPIC: "+numberOfTopic);
	}
	
	private void decrement(int did, int p){
		DocumentStatus ds = documentList[did];
		PhraseStatus ps = ds.phraseList[p];
		int hid = ps.getHid();
		int mid = ps.getMid();
		
		int tableAssignment = ps.tableAssignment;
		int topicAssignment = ps.topicAssignment;
		int sentimentAssignment = ps.sentimentAssignment;
		
		//table level
		documentList[did].tableList.get(tableAssignment).numberOfHeadWord--;
		documentList[did].tableList.get(tableAssignment).headWordVector.setEntry(p,0);
		//topic level
		topicList.get(topicAssignment).numberOfHeadWord--;
		topicList.get(topicAssignment).headWordList[hid]--;
		if(topicList.get(topicAssignment).headWordList[hid]==0){
			topicList.get(topicAssignment).headWordSet.remove((Object)hid);
		}
		//sentiment level
		topicList.get(topicAssignment).numberOfModiWord--;
		topicList.get(topicAssignment).sw[sentimentAssignment]--;
		topicList.get(topicAssignment).tsw[sentimentAssignment][mid]--;
		
		//if the table is empty, remove this table
		if(documentList[did].tableList.get(tableAssignment).numberOfHeadWord==0){
			documentList[did].removeTable(new TableStatus(tableAssignment,did));
		}
		
		//if the topic is empty, remove this topic
		if(topicList.get(topicAssignment).numberOfHeadWord==0){
			//change topic id
			for(int d=0;d<numberOfDocument;d++){
				DocumentStatus doc = documentList[d];
				List<TableStatus> tableList = doc.tableList;
				for(int t=0;t<tableList.size();t++){
					if(tableList.get(t).topicAssignment>topicAssignment){
						documentList[d].tableList.get(t).topicAssignment--;
					}
				}
				PhraseStatus[] phraseList = doc.phraseList;
				for(int i=0;i<phraseList.length;i++){
					if(phraseList[i].topicAssignment>topicAssignment){
						documentList[d].phraseList[i].topicAssignment--;
					}
				}
			}
			
			for(int t=topicAssignment+1;t<topicList.size();t++){
				topicList.get(t).tid--;
			}
			topicList.remove(topicAssignment);
			this.numberOfTopic--;
		}
	}
	
	private void allocate(int did, int p, int tableAssignment, int topicAssignment, int sentimentAssignment){
		DocumentStatus ds = documentList[did];
		PhraseStatus ps = ds.phraseList[p];
		int hid = ps.getHid();
		int mid = ps.getMid();
		//table level
		documentList[did].phraseList[p].tableAssignment=tableAssignment;
		documentList[did].phraseList[p].topicAssignment=topicAssignment;
		documentList[did].phraseList[p].sentimentAssignment=sentimentAssignment;
		
		if(tableAssignment==ds.numberOfTable){
			TableStatus ts = new TableStatus(tableAssignment,did,ds.getDocumentLength());
			ts.numberOfHeadWord++;
			ts.headWordVector.setEntry(p, 1);
			ts.topicAssignment=topicAssignment;
			documentList[did].addTable(ts);
			documentList[did].numberOfTable++;
		}else{
			documentList[did].tableList.get(tableAssignment).topicAssignment=topicAssignment;
			documentList[did].tableList.get(tableAssignment).headWordVector.setEntry(p, 1);
			documentList[did].tableList.get(tableAssignment).numberOfHeadWord++;
		}
		
		//topic level & sentiment level
		topicList.get(topicAssignment).numberOfHeadWord++;
		topicList.get(topicAssignment).numberOfModiWord++;
		topicList.get(topicAssignment).headWordList[hid]++;
		topicList.get(topicAssignment).tsw[sentimentAssignment][mid]++;
		topicList.get(topicAssignment).sw[sentimentAssignment]++;
		
	}
	
	private int tableSampling(int did, int p){
		
		/*
		 * Each word wil choose a table to sit
		 * Each word will scan over all the tables, and choose the most similar one to sit
		 * */
		
		DocumentStatus ds = documentList[did];
		PhraseStatus ps = ds.phraseList[p];
		int hid = ps.getHid();
		
		int table=-1;
		int numberOfTable = ds.numberOfTable;
		double[] tsim = new double[numberOfTable];
		double[] tdist= new double[numberOfTable];
		double[] tt= new double[numberOfTable+1];
		double pSum=0.0;
		double simSum=0;
		double u=0.0;
		
		ArrayRealVector simVector = ps.getSimVector();
		List<TableStatus> tableList = ds.tableList;
		for(int t=0;t<tableList.size();t++){
			TableStatus ts = tableList.get(t);
			int topicAssignment =ts.topicAssignment;
			ArrayRealVector phraseVector = ps.getSimVector();
			double sim=simVector.dotProduct(phraseVector)/((double)ts.numberOfHeadWord);
			tsim[t]=sim;
			simSum+=sim;
			//for one topic, this sum of dist value should be 1
			tdist[t]=(topicList.get(topicAssignment).headWordList[hid]+beta)/(topicList.get(topicAssignment).numberOfHeadWord+numberOfHeadWord*beta);
		}
		
		for(int t=0;t<tableList.size();t++){
			pSum+=(tsim[t]/(simSum+alpha))*tdist[t];
			tt[t]=pSum;
		}
		pSum+=(alpha/(simSum+alpha))*random.nextDouble();
		tt[numberOfTable]=pSum;
		u=random.nextDouble()*pSum;
		
		for(table=0;table<=numberOfTable;table++){
			if(tt[table]>=u) break;
		}
		return table;
	}
	
	private int topicSampling(int did, int p){
		/*
		 * This method is used to sample topic
		 * Each table will choose a dish to eat
		 * Each table will eat the dish which is shared by the most similar tables
		 * */
		
		int topic=-1;
		double simSum=0.0;
		double u=0.0;
		double tsim[] = new double[numberOfTopic];
		double tdist[] = new double[numberOfTopic];
		double tt[] = new double[numberOfTopic+1];
		double pSum=0.0;
		
		DocumentStatus ds = documentList[did];
		PhraseStatus ps = ds.phraseList[p];
		int hid = ps.getHid();

		ArrayRealVector simVector = SIMMATRIX.get(hid);
		ArrayRealVector topicVector = new ArrayRealVector(numberOfHeadWord);
		//iterate all the topic
		for(int t=0;t<topicList.size();t++){
			//long time1=System.nanoTime();
			TopicStatus ts = topicList.get(t);
			double sim=0.0;
			double[] twordList= ts.headWordList;
			topicVector=new ArrayRealVector(twordList);
			sim=simVector.dotProduct(topicVector)/((double)ts.numberOfHeadWord);
			simSum+=sim;
			tsim[t]=sim;
			tdist[t]=(twordList[hid]+beta)/(ts.numberOfHeadWord+numberOfHeadWord*beta);
			//long time2=System.nanoTime();
			//System.out.println(time2-time1);
		}
		
		for(int t=0;t<topicList.size();t++){
			pSum+=(tsim[t]/(simSum+gamma))*tdist[t];
			tt[t]=pSum;
		}
		
		//if the topic number is less than the limit, it can create the topic, otherwise, it can not
		
		if(numberOfTopic>=this.numberOfUpperLimitTopic) {
			u=random.nextDouble()*pSum;
			for(topic=0;topic<numberOfTopic;topic++){
				if(tt[topic]>=u) break;
			}
		}else{
			pSum+=(gamma/(simSum+gamma))*random.nextDouble();
			tt[numberOfTopic]=pSum;
			u=random.nextDouble()*pSum;
			for(topic=0;topic<=topicList.size();topic++){
				if(tt[topic]>=u) break;
			}
		}
		return topic;
	}
	
	private int sentimentSampling(int did, int p, int topicAssignment){
		int sentiment=-1;
		double pSum=0;
		double[] ss= new double[numberOfSentiment];
		double u;
		
		DocumentStatus ds = documentList[did];
		PhraseStatus ps = ds.phraseList[p];
		TopicStatus ts = topicList.get(topicAssignment);
		int mid = ps.getMid();
		
		delta[0]=ps.getPositive();
		delta[1]=ps.getNegative();
		delta[2]=ps.getNeutral();
		
		double deltaSum=delta[0]+delta[1]+delta[2];
		
		int[][] tsw = ts.tsw;
		int[] sw=ts.sw;
		
		for(int s=0;s<numberOfSentiment;s++){
			pSum+=((tsw[s][mid]+epsilon)/(sw[s]+numberOfModiWord*epsilon))*((sw[s]+delta[s])/(ts.numberOfModiWord+deltaSum));
			ss[s]=pSum;
		}
		u=random.nextDouble()*pSum;
		for(sentiment=0;sentiment<numberOfSentiment;sentiment++){
			if(ss[sentiment]>=u) break;
		}
		return sentiment;
	}
	
	public static void main(String[] args) throws IOException {
		String dir="D:/JavaProject/Sentiment Analysis/dataset/data/phrasemodel/data/";
		String fileAddr=dir+"phrase.txt";
		String headMap = dir+"headMap.txt";
		String modiMap = dir+"modifierMap.txt";
		String simFile = dir+"headSimilarity.txt";
		
		String sDir="D:/JavaProject/Sentiment Analysis/dataset/data/";
		String sentFile = sDir+"sentiment.txt";
		
		Documents documents = new Documents(fileAddr,headMap,modiMap,simFile,sentFile);
		int initK=10;
		int upperK=100;
		int initS=3;
		Model model = new Model(documents,initK,upperK,initS);
		GibbsSampling gs = new GibbsSampling(model);
	}

}
