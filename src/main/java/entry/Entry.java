package entry;

import java.io.IOException;

import phrasemodel.PhraseModel;
import wordmodel.WordModel;
/**
 * 
 */

/**
 * @author Wanying Ding
 * @email wd78@drexel.edu
 * @version Sep 25, 2014
 *
 */
public class Entry {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String commonDir="D:/JavaProject/Sentiment Analysis/dataset/data/";
		String phraseDataDir=commonDir+"phrasemodel/data/";
		String phraseResultDir = commonDir+"phrasemodel/result/";
		
		String wordDataDir=commonDir+"wordmodel/data/";
		String wordResultDir=commonDir+"wordmodel/result/";
		
		WordModel wm = new WordModel(commonDir,wordDataDir,wordResultDir);
		PhraseModel pm = new PhraseModel(commonDir, phraseDataDir, phraseResultDir);
		
		int initK=10;
		int upperK=100;
		int initS=3;
		wm.run(initK,upperK,initS);
		pm.run(initK,upperK,initS);
		
		

	}

}
